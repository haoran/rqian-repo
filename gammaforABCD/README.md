### This folder is for new sample for gammajet ABCD 

# get the histgram

1. finish the gird job

2. run find /path/to/sample/file/folder -type  f  -name "*.root" > xxx.txt to get the input of ReadingTree_xxx.py

3. change the cross section and gen_Fil in ReadingTree according to the mc_channel number. Values can be found in AMI.txt

4. Execute the following commands in a directory you will remember, or the working directory to install uproot to a virtual environment.

	python3 -m venv env
	source env/bin/activate
	pip install uproot3

set the path in ReadingTree_xxx.sh
5. mkdir output error log

6. condor_submit ReadingTree_xxx.sub

7. merge the output with hadd -f

8. run hist.py to get the histgram. The default lumi is 139fb-1(full Run2), rescale if necessary.

9. change the path in lxplus for every script

# possible problem now

1. abnormal event with large weight

2. wrong weight appied

3. many negative pu_weight(sherpa)
