#add data
from ROOT import *
import numpy as np

def myText(x,y,text,color=1):
    l = TLatex()
    l.SetTextSize(0.025)
    l.SetNDC()
    l.SetTextColor(color)
    l.DrawLatex(x,y,text)
    pass

var = "pt"
### sherpa ###
#file0 = TFile("ROOT/gammajet_sherpa.root")
#file1 = TFile("ROOT/dijet_sherpa_py_forGamma_full.root")

### pythia ###
file0 = TFile("/home/qrq/gammajet/root-files/gammajet_dijet_sherpa36.root")
#file1 = TFile("/home/qrq/gammatwojet/gammajet/root-files/dijet_pythia_fullrange.root")

### data ###
dijet_data_file  = TFile("/home/qrq/gammajet/root-files/dijet_data_py_forGamma.root")
gamma_data_file = TFile("/home/qrq/gammajet/root-files/gammajet_data.root")

gStyle.SetOptStat(0)
# for gamma jet
bin = np.asarray([0, 50, 100, 150, 200, 300, 400, 500, 600, 800, 1000, 1200, 1500, 2000])
zero_quark = file0.Get("0_LeadingJet_Central_Quark_pt")
zero_gluon = file0.Get("0_LeadingJet_Central_Gluon_pt")
zero_other = file0.Get("0_LeadingJet_Central_Other_pt")
zero_data = gamma_data_file.Get("0_LeadingJet_Central_Data_pt")
 
for i in range(1,13):
    gamma_quark = file0.Get(str(bin[i])+"_LeadingJet_Central_Quark_"+var)
    gamma_gluon = file0.Get(str(bin[i])+"_LeadingJet_Central_Gluon_"+var)
    gamma_other = file0.Get(str(bin[i]) + "_LeadingJet_Central_Other_" +var)
    gamma_data = gamma_data_file.Get(str(bin[i]) + "_LeadingJet_Central_Data_" +var)
    
    zero_other.Add(gamma_other)
    zero_gluon.Add(gamma_gluon)
    zero_quark.Add(gamma_quark)
    zero_data.Add(gamma_data)
print("quark")
for i in range(1,62):
    print(i,zero_quark.GetBinContent(i),zero_quark.GetBinLowEdge(i))
print("gluon")
for i in range(1,62):
    print(i,zero_gluon.GetBinContent(i),zero_gluon.GetBinLowEdge(i))
print("other")
for i in range(1,62):
    print(i,zero_other.GetBinContent(i),zero_other.GetBinLowEdge(i))
    
a = np.array([])
for i in range(1,62):
    if i != 39 and i != 40 and i!= 42 and i != 43:
        a = np.append(a , zero_quark.GetBinLowEdge(i))

zero_other = zero_other.Rebin(56,"zero_other",a)
zero_quark = zero_quark.Rebin(56,"zero_quark",a)
zero_gluon = zero_gluon.Rebin(56,"zero_gluon",a)

zero_other.SetBinContent(39,zero_other.GetBinContent(39)/3)
zero_quark.SetBinContent(39,zero_quark.GetBinContent(39)/3)
zero_gluon.SetBinContent(39,zero_gluon.GetBinContent(39)/3)

zero_other.SetBinContent(40,zero_other.GetBinContent(40)/3)
zero_quark.SetBinContent(40,zero_quark.GetBinContent(40)/3)
zero_gluon.SetBinContent(40,zero_gluon.GetBinContent(40)/3)
zero_gluon.Add(zero_other)
zero_quark.Add(zero_gluon)
gamma_total = zero_quark.Clone()
gamma_total_data = zero_data.Clone()

#gamma_total.Add(zero_gluon)
#gamma_total.Add(zero_other)

#scale by gamma data
leg2 = TLegend(.65,0.75,0.95,0.85)
leg2.AddEntry(gamma_total,"gamma total", "l")
leg2.AddEntry(zero_quark,"gamma Quark","f")
leg2.AddEntry(zero_gluon,"gamma Gluon","f")
leg2.AddEntry(zero_other,"gamma Other","f")
leg2.AddEntry(gamma_total_data,"gamma data","l")

leg2.SetBorderSize(0)
leg2.SetFillStyle(0)
c = TCanvas("","",500,500)


zero_gluon.SetLineColor(4)
zero_other.SetLineColor(4)
zero_gluon.SetLineWidth(3)
gamma_total.SetLineColor(12)
gamma_total.SetLineWidth(3)
zero_quark.SetFillColor(2)
zero_gluon.SetFillColor(3)
zero_other.SetFillColor(94)
gamma_total_data.SetLineColor(6)
gamma_total_data.SetLineWidth(3)
zero_quark.GetXaxis().SetTitle('Jet p_{T} [GeV]')
zero_quark.GetYaxis().SetTitle('Events')

gPad.SetLogy()

zero_quark.Draw("Hist")
zero_gluon.Draw("HIST  same")
gamma_total.Draw("HIST same")
zero_other.Draw("HIST same")
gamma_total_data.Draw("HIST same")

leg2.Draw("same")
myText(0.2,0.84,'#it{#bf{#scale[1.4]{#bf{ATLAS} Simulation Preliminary}}}')
c.Print("gamma_distribution_"+var+"_sherpa_data_all.pdf")
#c.Print("./plots/fraction_plots/gamma_distribution_"+var+"_sherpa.pdf")

