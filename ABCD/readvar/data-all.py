from ROOT import *
import numpy as np
import os
import collections

def FindBinIndex(jet_pt,ptbin):
	for i in range(len(ptbin)-1):
		if jet_pt >= ptbin[i] and jet_pt < ptbin[i+1]:
			return i

	#print "error: jet pT ",jet_pt,"outside the bin range"
	return -1

bins = [0, 50, 100, 150, 200, 300, 400, 500, 600, 800, 1000, 1200, 1500, 2000]

A = np.zeros(13)
B = np.zeros(13)
C = np.zeros(13)
D = np.zeros(13)
f = open("/afs/cern.ch/user/r/rqian/public/gamma2jet/ABCD/readvar/data-all.txt","r")
data = []

for line in f:
    data.append(line[:-1])
f.close()

for j in data:
    #print(j)
    open_file = TFile(str(j),"r")
    file_tree = open_file.nominal

    for i in file_tree:
            if len(i.ph_baseline_isotool_pass_fixedcuttight)!= 0 and i.jet_pt[0]/1000 > 40 and i.jet_pt[0]/1000 < 2000 and abs(i.jet_eta[0]) < 2.1 and len(i.ph_pt) != 0 and i.ph_pt[0]/1000 > 125 and abs(i.ph_eta[0])<2.37:
                index = FindBinIndex(((i.jet_pt[0])/1000),bins)
                if i.ph_baseline_ph_isTight[0] == 0 and i.ph_baseline_ph_isMedium[0] == 0 and i.ph_baseline_isotool_pass_fixedcuttight[0] == 0:
                    D[index] += 1
                if i.ph_baseline_ph_isTight[0] == 1 and i.ph_baseline_isotool_pass_fixedcuttight[0] == 0:
                    B[index] += 1
                if i.ph_baseline_ph_isTight[0] == 0 and i.ph_baseline_ph_isMedium[0] == 0 and i.ph_baseline_isotool_pass_fixedcuttight[0] == 1:
                    C[index] += 1
                if i.ph_baseline_ph_isTight[0] ==1 and i.ph_baseline_isotool_pass_fixedcuttight[0] == 1:
                    A[index] += 1
            #print(A)
 
print("result:")
print(A)
print(B)
print(C)
print(D)
