from ROOT import *
import numpy as np
import os
import collections

mc_channel_no = [423100,423101,423102,423103,423104,423105,423106,423107,423108,423109,423110,42311,423112]
xsec = [1.6715E+10,1.3965E+09,3.7954E+08,1.0621E+08,6.7046E+06,3.4426E+05,2.3729E+04,2.2869E+03,7.0203E+02,3.9345E+01,3.3277,3.2495E-01,3.1480E-02]
#pb
gen_Fil = [2.8747E-05,2.5099E-05,2.67056938015e-05,3.9379E-05,4.9477E-05,5.5861E-05,5.2889E-05,3.4898E-05,3.6931E-05,3.7122E-05,3.4295E-05,3.0262E-05,2.8632E-05]
def FindBinIndex(jet_pt,ptbin):
        for i in range(len(ptbin)-1):
                if jet_pt >= ptbin[i] and jet_pt < ptbin[i+1]:
                        return i

        #print "error: jet pT ",jet_pt,"outside the bin range"
        return -1

bins = [0, 50, 100, 150, 200, 300, 400, 500, 600, 800, 1000, 1200, 1500, 2000]

A = np.zeros(13)
B = np.zeros(13)
C = np.zeros(13)
D = np.zeros(13)
f = open("/afs/cern.ch/user/r/rqian/public/gamma2jet/ABCD/readvar/gamma_mc.txt","r")
data = []

for line in f:
    data.append(line[:-1])
f.close()

nentries = np.zeros(13)

for j in data:
    print(j)
    for i in range(len(mc_channel_no)):
        if ('.' + str(mc_channel_no[i])) in j:
            index = i
    open_file = TFile(str(j),"r")
    file_tree = open_file.nominal
    entries = file_tree.GetEntries()
    nentries[index] += entries
print(nentries)

for j in data:
    print(j)
    for i in range(len(mc_channel_no)):
        if ('.' + str(mc_channel_no[i])) in j:
            ind = i

    open_file = TFile(str(j),"r")
    file_tree = open_file.nominal
    for i in file_tree:
            total_weight = 139000* gen_Fil[ind] * i.pu_weight*xsec[ind]*i.mconly_weight/nentries[ind]
            if len(i.ph_baseline_isotool_pass_fixedcuttight)!= 0 and i.jet_pt[0]/1000 > 40 and i.jet_pt[0]/1000 < 2000 and abs(i.jet_eta[0]) < 2.1 and len(i.ph_pt) != 0 and i.ph_pt[0]/1000 > 125 and abs(i.ph_eta[0])<2.37:
                index = FindBinIndex((i.jet_pt[0])/1000,bins)
                if i.ph_baseline_ph_isTight[0] == 0 and i.ph_baseline_ph_isMedium[0] == 0 and i.ph_baseline_isotool_pass_fixedcuttight[0] == 0:
                    D[index] += total_weight
                if i.ph_baseline_ph_isTight[0] == 1 and i.ph_baseline_isotool_pass_fixedcuttight[0] == 0:
                    B[index] += total_weight
                if i.ph_baseline_ph_isTight[0] == 0 and i.ph_baseline_ph_isMedium[0] == 0 and i.ph_baseline_isotool_pass_fixedcuttight[0] == 1:
                    C[index] += total_weight
                if i.ph_baseline_ph_isTight[0] ==1 and i.ph_baseline_isotool_pass_fixedcuttight[0] == 1:
                    A[index] += total_weight

print("result:")
print(A)
print(B)
print(C)
print(D)
