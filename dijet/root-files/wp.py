#!/usr/bin/env python
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
import uproot3 as uproot
import numpy as np
import sys

#bins = [300, 400, 500, 600, 800, 1000, 1200, 1500, 2000]  #this bin range is for only dijet event
bins = [0, 50, 100, 150, 200, 300, 400, 500, 600, 800, 1000, 1200, 1500, 2000]  #this bin range is for gammajet+dijet event
HistMap = {}
JetList = []

xsec = [0.0,21400000000.0,89314000.0,9275100.0,55101.0,1631.4,128.4,27.212,0.206,0.036,0.0,0.0,0.0]
eff = [0.0,0.0014414,0.0051224,0.00056516,0.0014972,0.024206,0.01082,0.003618,0.015966,0.0033033,0.0,0.0,0.0]

	   
finput = open("sherpa_list.txt")
inputs = finput.read().splitlines()

nentries = np.zeros(13)
usefiles = []

for file in inputs:
    tr = uproot.open(file)["AntiKt4EMPFlow_dijet_insitu"]
    data = tr.array("mcChannelNumber")
    print(data)
    index = (data[0]-76) % 10
    print(file,index)
    if(index >=1 and index <=4):
        entries = tr.numentries
        nentries[index] += entries
        print("using entries")
    if(index > 4):
        mc_mod = data[0] % 10
        entries = uproot.open(file)["AntiKt4EMPFlow_J"+str(mc_mod)+"_sumOfWeights"].values[0]
        nentries[index] += entries
        print("using weighted entries")
    usefiles.append(file)
        
print(nentries)

def GetJetType(label):
	if label == -99:
		return "Data"
	elif label == 21:
		return "Gluon"
	elif label > 0 and label < 5:
		return "Quark"
	else:
		return "Other"

bdt_list = np.array([])
w_list = np.array([])

#Unfourtunately I can't fully utilize the use of arrays because each jet must be matched with the corresponding histogram.
#for i in range():
def ReadTree(df,f):
    global bdt_list
    global w_list
    
    tr = uproot.open(f)["AntiKt4EMPFlow_dijet_insitu"]
    data = tr.array("mcChannelNumber")
    index = (data[0]-76) % 10
    print(f,index)
    if(index >=1 and index <=4):
        print("using entries")
    if(index > 4):
        print("using weighted entries")
    
    for i in range(0,len(df["pass_HLT_j420"])):
        if(df["pass_HLT_j420"][i] == 1 and df["j1_pT"][i] > 1500 and df["j1_pT"][i] < 2000 and abs(df["j1_eta"][i]) < 2.1 and abs(df["j2_eta"][i]) < 2.1 and df["j1_pT"][i]/df["j2_pT"][i] < 1.5):
            
            label1 = GetJetType(df["j1_partonLabel"][i])
            label2 = GetJetType(df["j2_partonLabel"][i])
            	
            total_weight = df["weight"][i]*xsec[index]*eff[index]/nentries[index]
            
            if(label1 == "Quark"):
                bdt_list = np.append(bdt_list,df["j1_bdt_resp"][i],axis=None)
                w_list = np.append(w_list,total_weight,axis=None)
            if(label2 == "Quark"):
                bdt_list = np.append(bdt_list,df["j2_bdt_resp"][i],axis=None)
                w_list = np.append(w_list,total_weight,axis=None)

######## read and excute TTree from root file 
#finput = TFile.Open("/eos/user/e/esaraiva/AQT_dijet_sherpa_bdt/dijet_sherpa_bdt_d.root")

#print(usefiles)
for file in usefiles:
    print(file)
    tr = uproot.open(file)["AntiKt4EMPFlow_dijet_insitu"]
    data = tr.arrays(["pass_HLT_j420","j[12]_pT","j[12]_eta","j[12]_bdt_resp","weight","j[12]_partonLabel"])#this converts the tree to a     python dictionary with keys equal to the branch names and values equal to the branch data. This can be left blank to iport all of the data from all branches.
    ReadTree(data,file)

w_list = [x for _,x in np.sort(zip(bdt_list,w_list))]
bdt_list = np.sort(bdt_list)
w_list = np.cumsum(w_list)
idx = (np.abs(w_list - 0.6*w_list[len(w_list)-1])).argmin()
wp = bdt_list[idx]
print(wp)

