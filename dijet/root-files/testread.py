from ROOT import *
import sys
import numpy as np
data = TFile("dijet_data_py.root")
data_tag = TFile("dijet_data_py_wp.root")

H1 = data.Get("1000_LeadingJet_Central_Gluon_bdt")
H2 = data_tag.Get("1000_LeadingJet_Central_Gluon_Data_bdt")

for i in range(1,61):
    print(i,H1.GetBinContent(i),H2.GetBinContent(i))
