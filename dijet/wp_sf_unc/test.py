###dijet
from ROOT import *
import numpy as np

# When quark efficiency is 0.6 ,using sherpa dijet sample as normalization ,calculating higer mc/higher data as scale factor, plotting scale factor , gluon rejection, and uncertainty. 

doreweight = 0   #decide if we want to do the reweighting process

var = "bdt"  #change the var name according to the inputvar you want to read
mc = "sherpa_SF"   #by setting it as "SF" or "MC", it will automatically making scale factor plots or MC closure plots
inputvar = var  #by setting it as bdt (or ntrk,width,c1..), it will read the corresponding histogram, but remember to change the TLine range according to X-axis of different variable, one can check it by browsing the histograms in root file.
def myText(x, y, text, color=1):
    l = TLatex()
    l.SetTextSize(0.025)
    l.SetNDC()
    l.SetTextColor(color)
    l.DrawLatex(x, y, text)
    pass


nstrap = 5000

qeff = np.array([])
grej = np.array([])
qsf_array = np.array([])
gsf_array = np.array([])
unc_array_q = np.array([])
unc_array_g = np.array([])


# return the abstive uncertainty of input his
def bootstrap(hist):
    errorhis = hist.Clone("")
    for i in range(1, hist.GetNbinsX() + 1):
        unc_array = np.array([])
        for j in range(nstrap):
            mean = hist.GetBinContent(i)
            unc_array = np.append(unc_array,np.random.poisson(mean))
        sigma = np.std(unc_array)
        errorhis.SetBinContent(i, sigma)
    return (errorhis)


# for checking bin content
def printhis(his):
    for i in range(1, 61):
        print(his.GetBinContent(i))


def abs_unc(sherpa, other):
    unc_use = sherpa.Clone("")
    for i in range(1, sherpa.GetNbinsX() + 1):
            unc_use.SetBinContent(i, np.abs(sherpa.GetBinContent(i) - other.GetBinContent(i)))
    return (unc_use)


def wp_bin(quark_mc, gluon_mc, data):
    wp = 0.6 * quark_mc.Integral(1, 61)
    x = np.empty(60)
    y = np.empty(60)
    for l in range(1, quark_mc.GetNbinsX() + 1):
        x[l - 1] = (quark_mc.Integral(1, l))
        y[l - 1] = (gluon_mc.Integral(1, l))
        if x[l - 1] > wp:
            wpointq = int(l)
            sf_q = data.Integral(1, l) / quark_mc.Integral(1, l)
            sf_g = data.Integral(l, 61) / gluon_mc.Integral(l, 61)
            qeff = quark_mc.Integral(1, l) / quark_mc.Integral(1, 61)
            grej = gluon_mc.Integral(1, l) / gluon_mc.Integral(1, 61)
            break
    return (l, sf_q, sf_g, qeff,grej)


sherpa_scale = 46769497.0/ (443.3592594806105)



c1 = TCanvas("","",500,500)

bin = np.array([500,600,800,1000,1200,1500,2000])

sherpa = TFile('../root-files/dijet_sherpa_sys_py.root')
data = TFile('../root-files/dijet_data_py.root')


qsf = np.array([])
gsf = np.array([])

for i in range(0,6):
    #print(type(sherpa))
    sherpa_gluon = sherpa.Get(str(bin[i])+'_LeadingJet_Forward_Gluon_bdt')
    sherpa_gluon2 = sherpa.Get(str(bin[i])+'_LeadingJet_Central_Gluon_bdt')
    sherpa_gluon3 = sherpa.Get(str(bin[i])+'_SubJet_Forward_Gluon_bdt')
    sherpa_gluon4 = sherpa.Get(str(bin[i])+'_SubJet_Central_Gluon_bdt')

    sherpa_gluon.Add(sherpa_gluon2)
    sherpa_gluon.Add(sherpa_gluon3)
    sherpa_gluon.Add(sherpa_gluon4)


    sherpa_quark = sherpa.Get(str(bin[i])+'_LeadingJet_Forward_Quark_bdt')
    sherpa_quark2 = sherpa.Get(str(bin[i])+'_LeadingJet_Central_Quark_bdt')
    sherpa_quark3 = sherpa.Get(str(bin[i])+'_SubJet_Forward_Quark_bdt')
    sherpa_quark4 = sherpa.Get(str(bin[i])+'_SubJet_Central_Quark_bdt')


    sherpa_quark.Add(sherpa_quark2)
    sherpa_quark.Add(sherpa_quark3)
    sherpa_quark.Add(sherpa_quark4)

    data1 = data.Get(str(bin[i])+'_LeadingJet_Forward_Data_bdt')
    data2 = data.Get(str(bin[i])+'_LeadingJet_Central_Data_bdt')
    data3 = data.Get(str(bin[i])+'_SubJet_Forward_Data_bdt')
    data4 = data.Get(str(bin[i])+'_SubJet_Central_Data_bdt')

    data1.Add(data2)
    data1.Add(data3)
    data1.Add(data4)

    #reassigning to variables
    higher_quark = sherpa_quark.Clone("")
    higher_gluon = sherpa_gluon.Clone("")
    higher_data = data1.Clone("")

    """
    #Get Wp SF only from higher 
    
    higher_quark_pythia = gamma_quark_pythia.Clone("")
    higher_gluon_pythia = gamma_gluon_pythia.Clone("")
    """
    ###need a rescale here
 
    higher_quark.Scale(sherpa_scale)
    higher_gluon.Scale(sherpa_scale)
    ##higher_quark_pythia.Scale(pythia_scale)
    ##higher_gluon_pythia.Scale(pythia_scale)

    l,sf_q,sf_g,qeff_bin,grej_bin = wp_bin(higher_quark,higher_gluon,higher_data)
    print(l,sf_q,sf_g,qeff_bin,grej_bin)
    """
    ###bootstrap
    data_bootstrap = bootstrap(higher_data)
    
    ###pythia
    higher_quark_pythia_unc = abs_unc(higher_quark,higher_quark_pythia)
    higher_gluon_pythia_unc = abs_unc(higher_gluon,higher_gluon_pythia)

            
    sum_unc_q = np.zeros(60)
    sum_unc_g = np.zeros(60)
    mc_unc_q = higher_quark.Clone("")
    mc_unc_g = higher_gluon.Clone("")
            #print(m)
    for i in range(1,higher_quark.GetNbinsX()+1):
                sum_unc_q[i-1] = np.sqrt(quark_bootstrap.GetBinContent(i)**2 + higher_quark_pythia_unc.GetBinContent(i)**2)
                sum_unc_g[i-1] = np.sqrt(gluon_bootstrap.GetBinContent(i)**2 + higher_gluon_pythia_unc.GetBinContent(i)**2)
                mc_unc_q.SetBinContent(i,sum_unc_q[i-1])
                mc_unc_g.SetBinContent(i,sum_unc_g[i-1])

            #l,sf_q,sf_g,qeff_bin,grej_bin = wp_bin(higher_quark,higher_gluon,higher_data)
    
    unc_q = 0
    unc_g = 0
    bin_q = 0
    bin_g = 0
    unc_d = 0
    bin_d = 0
    for i in range(int(l),61):
                a = mc_unc_q.GetBinContent(i)**2
                b = higher_gluon.GetBinContent(i)
                unc_d += data_bootstrap.GetBinContent(l)**2
                bin_d += higher_data.GetBinContent(l) 
                unc_g += a
                bin_g += b
            
    for i in range(int(l),61):
                c = mc_unc_q.GetBinContent(i)**2
                d = higher_quark.GetBinContent(i) 
                unc_q += c
                bin_q += d
            
    re_unc_q = np.sqrt((np.sqrt(unc_q)/bin_q)**2 +(np.sqrt(unc_d)/bin_d)**2 )
    re_unc_g = np.sqrt((np.sqrt(unc_g)/bin_g)**2 +(np.sqrt(unc_d)/bin_d)**2 )
    unc_array_q = np.append(unc_array_q,re_unc_q)
    unc_array_g = np.append(unc_array_g,re_unc_g)
    """
    qeff = np.append(qeff,qeff_bin)
    grej = np.append(grej,grej_bin)
    qsf_array = np.append(qsf_array,sf_q)
    gsf_array = np.append(gsf_array,sf_g)
    print(l)
def myText(x,y,text, color = 1):
	l = TLatex()
	l.SetTextSize(0.025)
	l.SetNDC()
	l.SetTextColor(color)
	l.DrawLatex(x,y,text)
	pass


bins = np.array([500.,600.,800.,1000.,1200.,1500.,2000.])

gStyle.SetOptStat(0)
c = TCanvas("","",500,500)
#gPad.SetTickx()
#gPad.SetTicky()
rmax = 1.5

histq = TH1F("","",6,bins)
histg = TH1F("","",6,bins)
histqsf = TH1F("","",6,bins)
histgsf = TH1F("","",6,bins)

for i in range(0,6):
    histq.SetBinContent(i+1,qeff[i])
    histg.SetBinContent(i+1,grej[i])
    histqsf.SetBinContent(i+1,qsf_array[i])
    histgsf.SetBinContent(i+1,gsf_array[i])


histq.SetMaximum(1.)
histq.SetMinimum(0.)
histq.GetXaxis().SetRangeUser(500.,2000.)
histq.GetXaxis().SetTitle("Jet p_{T} (GeV)")
histq.GetYaxis().SetTitle("Efficiency")

histq.SetMarkerStyle(24)
histq.SetLineColor(4)
histq.SetMarkerColor(1)
histq.SetMarkerSize(1)

histg.SetLineColor(2)
histg.SetMarkerStyle(32)
histg.SetMarkerSize(1)
histg.SetMarkerColor(1)

histqsf.SetLineColor(4)
histqsf.SetMarkerStyle(24)
histqsf.SetMarkerSize(1)
histqsf.SetMarkerColor(1)
histqsf.SetLineStyle(7)
histqsf.SetMaximum(10)

histgsf.SetLineColor(2)
histgsf.SetMarkerStyle(32)
histgsf.SetMarkerSize(1)
histgsf.SetMarkerColor(1)
histgsf.SetLineStyle(7)
histgsf.SetMaximum(10)
scale = 1/rmax
histqsf.Scale(scale)
histgsf.Scale(scale)

leg = TLegend(0.6,0.6,0.84,0.84)
leg.SetTextFont(42)
leg.SetFillColor(0)
leg.SetBorderSize(0)
leg.SetFillStyle(0)
leg.AddEntry(histq,"Quark Efficiency","lp")
leg.AddEntry(histqsf,"Quark Scale Factor","lp")
leg.AddEntry(histg,"Gluon Rejection","lp")
leg.AddEntry(histgsf,"Gluon Scale Factor","lp")

histq.Draw("L P0")
histg.Draw("L P0 same")
histqsf.Draw("hist L P0 same")
histgsf.Draw("hist L P0 same")
leg.Draw("same")
myText(0.18,0.84,"#it{#bf{#scale[1.8]{#bf{ATLAS} Simulation Internal}}}")

axis = TGaxis(2000.,0.,2000.,1.,0.,rmax,510,"+L")
axis.SetTitle("Scale factor")
axis.Draw()
print(qsf_array)
print(gsf_array)
c.Print("testsys.pdf")
