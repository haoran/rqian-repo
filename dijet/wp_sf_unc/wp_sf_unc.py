###dijet
from ROOT import *
import numpy as np

# When quark efficiency is 0.6 ,using sherpa dijet sample as normalization ,calculating higer mc/higher data as scale factor, plotting scale factor , gluon rejection, and uncertainty. 

doreweight = 0   #decide if we want to do the reweighting process

var = "bdt"  #change the var name according to the inputvar you want to read
mc = "sherpa_SF"   #by setting it as "SF" or "MC", it will automatically making scale factor plots or MC closure plots
inputvar = var  #by setting it as bdt (or ntrk,width,c1..), it will read the corresponding histogram, but remember to change the TLine range according to X-axis of different variable, one can check it by browsing the histograms in root file.

def myText(x,y,text, color = 1):
	l = TLatex()
	l.SetTextSize(0.025)
	l.SetNDC()
	l.SetTextColor(color)
	l.DrawLatex(x,y,text)
	pass

nstrap =5000

qeff = np.array([])
grej = np.array([])
qsf_array = np.array([])
gsf_array = np.array([]) 
unc_array_q = np.array([]) 
unc_array_g = np.array([]) 


#return the abstive uncertainty of input his
def bootstrap(hist):
    errorhis = hist.Clone("")
    for i in range(1,hist.GetNbinsX()+1):
        unc_array = np.array([])
        for j in range(nstrap):
            mean = hist.GetBinContent(i)
            if mean < 0:
                mean = 0 
            unc_array=np.append(unc_array,np.random.poisson(mean))
        unc_array.sort()
        median = np.median(unc_array)
        if median == 0:
            errorhis.SetBinContent(i,0)
        else:
            sigma = np.std(unc_array)
            errorhis.SetBinContent(i,sigma)
    return(errorhis)


# for checking bin content 
def printhis(his):
    for i in range(1,61):
        print(his.GetBinContent(i))

def abs_unc(sherpa,other):
    unc_use = sherpa.Clone("")
    for i in range(1,sherpa.GetNbinsX()+1):
        if sherpa.GetBinContent(i) == 0 and other.GetBinContent(i) == 0:
            unc_use.SetBinContent(i,0)
        else:
            unc_use.SetBinContent(i,np.abs(sherpa.GetBinContent(i)-other.GetBinContent(i)))
    return(unc_use)    

def wp_bin(quark_mc,gluon_mc,data):
    wp = 0.6*quark_mc.Integral(1,61)
    x = np.empty(60)
    y = np.empty(60)
    for l in range(1,quark_mc.GetNbinsX()+1):
        x[l-1] = (quark_mc.Integral(1,l))
        y[l-1] = (gluon_mc.Integral(1,l))
        if x[l-1] > wp:
            wpointq = int(l)
            sf_q = data.Integral(l,61)/quark_mc.Integral(l,61)
            sf_g = data.Integral(l,61)/gluon_mc.Integral(l,61)
            break
    return(l,sf_q,sf_g,quark_mc.Integral(1,l)/quark_mc.Integral(1,61),gluon_mc.Integral(1,l)/gluon_mc.Integral(1,61))

c1 = TCanvas("","",500,500)

bin = [0, 50, 100, 150, 200, 300, 400, 500, 600, 800, 1000, 1200, 1500, 2000]

gammasherpa = TFile("../root-files/gammajet_sherpa.root")
gammadata = TFile("../root-files/gammajet_data.root")
gammapythia = TFile("../root-files/gammajet_pythia.root")



sherpa_scale = 21819398.0/685039.4795847898
pythia_scale = 21819398.0/597255.4938077908


qsf = np.array([])
gsf = np.array([])

for i in range(0,13):   #for only dijet event, start from jet pT>500 GeV
#for i in range(13):	#for gamma+jet combined with dijet event, start from jet pT>0 GeV
    min = bin[i]
    max = bin[i+1]
    print(i)
    #sherpa

    gamma_quark = gammasherpa.Get(str(min) + "_LeadingJet_Central_Quark_" + inputvar)
    gamma_gluon = gammasherpa.Get(str(min) + "_LeadingJet_Central_Gluon_" + inputvar)
    #pythia
    gamma_quark_pythia = gammapythia.Get(str(min) + "_LeadingJet_Central_Quark_" + inputvar)
    gamma_gluon_pythia = gammapythia.Get(str(min) + "_LeadingJet_Central_Gluon_" + inputvar)
    #data
    gamma_data = gammadata.Get(str(min)+"_LeadingJet_Central_Data_"+inputvar)


    #reassigning to variables
    higher_quark = gamma_quark.Clone("")
    higher_gluon = gamma_gluon.Clone("")
    higher_data = gamma_data.Clone("")


    #Get Wp SF only from higher 
    
    higher_quark_pythia = gamma_quark_pythia.Clone("")
    higher_gluon_pythia = gamma_gluon_pythia.Clone("")

    ###need a rescale here
 
    higher_quark.Scale(sherpa_scale)
    higher_gluon.Scale(sherpa_scale)
    higher_quark_pythia.Scale(pythia_scale)
    higher_gluon_pythia.Scale(pythia_scale)

    l,sf_q,sf_g,qeff_bin,grej_bin = wp_bin(higher_quark,higher_gluon,higher_data)
    ###bootstrap
    quark_bootstrap = bootstrap(higher_quark)
    gluon_bootstrap = bootstrap(higher_gluon)
    data_bootstrap = bootstrap(higher_data)

    ###pythia
    higher_quark_pythia_unc = abs_unc(higher_quark,higher_quark_pythia)
    higher_gluon_pythia_unc = abs_unc(higher_gluon,higher_gluon_pythia)

            
    sum_unc_q = np.zeros(60)
    sum_unc_g = np.zeros(60)
    mc_unc_q = higher_quark.Clone("")
    mc_unc_g = higher_gluon.Clone("")
            #print(m)
    for i in range(1,higher_quark.GetNbinsX()+1):
                sum_unc_q[i-1] = np.sqrt(quark_bootstrap.GetBinContent(i)**2 + higher_quark_pythia_unc.GetBinContent(i)**2)
                sum_unc_g[i-1] = np.sqrt(gluon_bootstrap.GetBinContent(i)**2 + higher_gluon_pythia_unc.GetBinContent(i)**2)
                mc_unc_q.SetBinContent(i,sum_unc_q[i-1])
                mc_unc_g.SetBinContent(i,sum_unc_g[i-1])

            #l,sf_q,sf_g,qeff_bin,grej_bin = wp_bin(higher_quark,higher_gluon,higher_data)
    unc_q = 0
    unc_g = 0
    bin_q = 0
    bin_g = 0
    unc_d = 0
    bin_d = 0
    for i in range(int(l),61):
                a = mc_unc_q.GetBinContent(i)**2
                b = higher_gluon.GetBinContent(i)
                unc_d += data_bootstrap.GetBinContent(l)**2
                bin_d += higher_data.GetBinContent(l) 
                unc_g += a
                bin_g += b
            
    for i in range(int(l),61):
                c = mc_unc_q.GetBinContent(i)**2
                d = higher_quark.GetBinContent(i) 
                unc_q += c
                bin_q += d
            
    re_unc_q = np.sqrt((np.sqrt(unc_q)/bin_q)**2 +(np.sqrt(unc_d)/bin_d)**2 )
    re_unc_g = np.sqrt((np.sqrt(unc_g)/bin_g)**2 +(np.sqrt(unc_d)/bin_d)**2 )
    unc_array_q = np.append(unc_array_q,re_unc_q)
    unc_array_g = np.append(unc_array_g,re_unc_g)
    qeff = np.append(qeff,qeff_bin)
    grej = np.append(grej,grej_bin)
    qsf_array = np.append(qsf_array,sf_q)
    gsf_array = np.append(gsf_array,sf_g)
    print(l)
def myText(x,y,text, color = 1):
	l = TLatex()
	l.SetTextSize(0.025)
	l.SetNDC()
	l.SetTextColor(color)
	l.DrawLatex(x,y,text)
	pass

bins = np.array([0.,50.,100.,150.,200.,300.,400.,500])


gStyle.SetOptStat(0)
c = TCanvas("","",500,500)
#gPad.SetTickx()
#gPad.SetTicky()
rmax = 3

histq = TH1F("","",7,bins)
histg = TH1F("","",7,bins)
histqsf = TH1F("","",7,bins)
histgsf = TH1F("","",7,bins)

for i in range(0,7):
    histq.SetBinContent(i+1,qeff[i])
    histg.SetBinContent(i+1,grej[i])
    histqsf.SetBinContent(i+1,qsf_array[i])
    histgsf.SetBinContent(i+1,gsf_array[i])
    histqsf.SetBinError(i+1,qsf_array[i]*unc_array_q[i])
    histgsf.SetBinError(i+1,gsf_array[i]*unc_array_g[i])

histq.SetMaximum(1.)
histq.SetMinimum(0.)
histq.GetXaxis().SetRangeUser(0.,500.)
histq.GetXaxis().SetTitle("Jet p_{T} (GeV)")
histq.GetYaxis().SetTitle("Efficiency")

histq.SetMarkerStyle(24)
histq.SetLineColor(4)
histq.SetMarkerColor(1)
histq.SetMarkerSize(1)

histg.SetLineColor(2)
histg.SetMarkerStyle(32)
histg.SetMarkerSize(1)
histg.SetMarkerColor(1)

histqsf.SetLineColor(4)
histqsf.SetMarkerStyle(24)
histqsf.SetMarkerSize(1)
histqsf.SetMarkerColor(1)
histqsf.SetLineStyle(7)
histqsf.SetMaximum(10)

histgsf.SetLineColor(2)
histgsf.SetMarkerStyle(32)
histgsf.SetMarkerSize(1)
histgsf.SetMarkerColor(1)
histgsf.SetLineStyle(7)
histgsf.SetMaximum(10)
scale = 1/rmax
histqsf.Scale(scale)
histgsf.Scale(scale)

leg = TLegend(0.6,0.6,0.84,0.84)
leg.SetTextFont(42)
leg.SetFillColor(0)
leg.SetBorderSize(0)
leg.SetFillStyle(0)
leg.AddEntry(histq,"Quark Efficiency","lp")
leg.AddEntry(histqsf,"Quark Scale Factor","lp")
leg.AddEntry(histg,"Gluon Rejection","lp")
leg.AddEntry(histgsf,"Gluon Scale Factor","lp")

histq.Draw("L P0")
histg.Draw("L P0 same")
histqsf.Draw("hist L P0 same E")
histgsf.Draw("hist L P0 same E")
leg.Draw("same")
myText(0.18,0.84,"#it{#bf{#scale[1.8]{#bf{ATLAS} Simulation Internal}}}")

axis = TGaxis(500.,0.,500.,1.,0.2,rmax,510,"+L")#
axis.SetTitle("Scale factor")
axis.Draw()
print(qeff)
print(qsf_array)
print(unc_array_q)
c.Print("testsys.pdf")
