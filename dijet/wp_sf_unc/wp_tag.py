from ROOT import *
import numpy as np


def myText(x,y,text, color = 1):
	l = TLatex()
	l.SetTextSize(0.025)
	l.SetNDC()
	l.SetTextColor(color)
	l.DrawLatex(x,y,text)
	pass



bins = np.array([500.,600.,800.,1000.,1200.,1500.,2000.])
bin = np.array([500,600,800,1000,1200,1500,2000])
qeff = [0.60000,0.59999,0.60000,0.59999,0.60000,0.60000]
grej = [0.16977,0.16416,0.15935,0.15481,0.14677,0.14660]
#qsf = [1.076,1.076,1.067,0.986,1.007,0.837] #Left
#gsf = [0.926,0.918,0.933,1.010,1.083,1.998] #Left
#qsf = [-39.,3.54,3.16,0.92,0.85,0.47] #Right
#gsf = [1.34,1.22,1.20,0.93,0.90,0.75] #Right
#qsf = [0.801,0.821,0.804,0.932,0.921,0.969] #Right, no wp cut
#gsf = [1.187,1.116,1.091,0.918,0.885,0.783] #Right, no wp cut

sherpa = TFile('../root-files/dijet_sherpa_py_tag.root')
data = TFile('../root-files/dijet_data_py_wp.root')
sherpa_lund = TFile('../root-files/dijet_sherpa_lund_py_tag.root')
powpyt = TFile('../root-files/dijet_powpyt_py_tag.root')
pythia = TFile('../root-files/dijet_pythia_a_py_tag.root')
herdipo = TFile('../root-files/dijet_herdipo_py_tag.root')
herang = TFile('../root-files/dijet_herang_a_py_tag.root')

gStyle.SetOptStat(0)
c = TCanvas("","",500,500)
#gPad.SetTickx()
#gPad.SetTicky()
rmax = 1.5

histq = TH1F("","",6,bins)
histg = TH1F("","",6,bins)
histqsf = TH1F("","",6,bins)
histgsf = TH1F("","",6,bins)
qhadronization = TH1F("","",6,bins)
ghadronization = TH1F("","",6,bins)

n_data = 46769496
n_sherpa = 441.1985
n_sherpa_lund = 434.272377668*36000
n_powpyt = 20668826.
n_pythia =  20478276.#a+e+d: 79598300.
n_herang = 16180651.0 #ignore:620740.
n_herdipo = 23635070.

scale = n_data/n_sherpa
scale_lund = n_data/n_sherpa_lund
scale_herang = n_data/n_herang
scale_herdipo = n_data/n_herdipo
scale_pythia = n_data/n_pythia
scale_powpyt = n_data/n_powpyt
print(scale_powpyt)

for i in range(0,6):
    #print(type(sherpa))
    sherpa_gluon = sherpa.Get(str(bin[i])+'_LeadingJet_Forward_Gluon_bdt')
    data_gluon = data.Get(str(bin[i])+'_LeadingJet_Forward_Gluon_Data_bdt')
    sherpa_gluon2 = sherpa.Get(str(bin[i])+'_LeadingJet_Central_Gluon_bdt')
    data_gluon2 = data.Get(str(bin[i])+'_LeadingJet_Central_Gluon_Data_bdt')
    sherpa_gluon3 = sherpa.Get(str(bin[i])+'_SubJet_Forward_Gluon_bdt')
    data_gluon3 = data.Get(str(bin[i])+'_SubJet_Forward_Gluon_Data_bdt')
    sherpa_gluon4 = sherpa.Get(str(bin[i])+'_SubJet_Central_Gluon_bdt')
    data_gluon4 = data.Get(str(bin[i])+'_SubJet_Central_Gluon_Data_bdt')

    sherpa_gluon.Add(sherpa_gluon2)
    sherpa_gluon.Add(sherpa_gluon3)
    sherpa_gluon.Add(sherpa_gluon4)
    data_gluon.Add(data_gluon2)
    data_gluon.Add(data_gluon3)
    data_gluon.Add(data_gluon4)

    sherpa_quark = sherpa.Get(str(bin[i])+'_LeadingJet_Forward_Quark_bdt')
    data_quark = data.Get(str(bin[i])+'_LeadingJet_Forward_Quark_Data_bdt')
    sherpa_quark2 = sherpa.Get(str(bin[i])+'_LeadingJet_Central_Quark_bdt')
    data_quark2 = data.Get(str(bin[i])+'_LeadingJet_Central_Quark_Data_bdt')
    sherpa_quark3 = sherpa.Get(str(bin[i])+'_SubJet_Forward_Quark_bdt')
    data_quark3 = data.Get(str(bin[i])+'_SubJet_Forward_Quark_Data_bdt')
    sherpa_quark4 = sherpa.Get(str(bin[i])+'_SubJet_Central_Quark_bdt')
    data_quark4 = data.Get(str(bin[i])+'_SubJet_Central_Quark_Data_bdt')

    sherpa_quark.Add(sherpa_quark2)
    sherpa_quark.Add(sherpa_quark3)
    sherpa_quark.Add(sherpa_quark4)
    data_quark.Add(data_quark2)
    data_quark.Add(data_quark3)
    data_quark.Add(data_quark4)

    sherpa_lund_gluon = sherpa_lund.Get(str(bin[i])+'_LeadingJet_Forward_Gluon_bdt')
    sherpa_lund_gluon2 = sherpa_lund.Get(str(bin[i])+'_LeadingJet_Central_Gluon_bdt')
    sherpa_lund_gluon3 = sherpa_lund.Get(str(bin[i])+'_SubJet_Forward_Gluon_bdt')
    sherpa_lund_gluon4 = sherpa_lund.Get(str(bin[i])+'_SubJet_Central_Gluon_bdt')

    sherpa_lund_gluon.Add(sherpa_lund_gluon2)
    sherpa_lund_gluon.Add(sherpa_lund_gluon3)
    sherpa_lund_gluon.Add(sherpa_lund_gluon4)

    sherpa_lund_quark = sherpa_lund.Get(str(bin[i])+'_LeadingJet_Forward_Quark_bdt')
    sherpa_lund_quark2 = sherpa_lund.Get(str(bin[i])+'_LeadingJet_Central_Quark_bdt')
    sherpa_lund_quark3 = sherpa_lund.Get(str(bin[i])+'_SubJet_Forward_Quark_bdt')
    sherpa_lund_quark4 = sherpa_lund.Get(str(bin[i])+'_SubJet_Central_Quark_bdt')

    sherpa_lund_quark.Add(sherpa_lund_quark2)
    sherpa_lund_quark.Add(sherpa_lund_quark3)
    sherpa_lund_quark.Add(sherpa_lund_quark4)

    herang_gluon = herang.Get(str(bin[i])+'_LeadingJet_Forward_Gluon_bdt')
    herang_gluon2 = herang.Get(str(bin[i])+'_LeadingJet_Central_Gluon_bdt')
    herang_gluon3 = herang.Get(str(bin[i])+'_SubJet_Forward_Gluon_bdt')
    herang_gluon4 = herang.Get(str(bin[i])+'_SubJet_Central_Gluon_bdt')

    herang_gluon.Add(herang_gluon2)
    herang_gluon.Add(herang_gluon3)
    herang_gluon.Add(herang_gluon4)

    herang_quark = herang.Get(str(bin[i])+'_LeadingJet_Forward_Quark_bdt')
    herang_quark2 = herang.Get(str(bin[i])+'_LeadingJet_Central_Quark_bdt')
    herang_quark3 = herang.Get(str(bin[i])+'_SubJet_Forward_Quark_bdt')
    herang_quark4 = herang.Get(str(bin[i])+'_SubJet_Central_Quark_bdt')

    herang_quark.Add(herang_quark2)
    herang_quark.Add(herang_quark3)
    herang_quark.Add(herang_quark4)

    herdipo_gluon = herdipo.Get(str(bin[i])+'_LeadingJet_Forward_Gluon_bdt')
    herdipo_gluon2 = herdipo.Get(str(bin[i])+'_LeadingJet_Central_Gluon_bdt')
    herdipo_gluon3 = herdipo.Get(str(bin[i])+'_SubJet_Forward_Gluon_bdt')
    herdipo_gluon4 = herdipo.Get(str(bin[i])+'_SubJet_Central_Gluon_bdt')

    herdipo_gluon.Add(herdipo_gluon2)
    herdipo_gluon.Add(herdipo_gluon3)
    herdipo_gluon.Add(herdipo_gluon4)

    herdipo_quark = herdipo.Get(str(bin[i])+'_LeadingJet_Forward_Quark_bdt')
    herdipo_quark2 = herdipo.Get(str(bin[i])+'_LeadingJet_Central_Quark_bdt')
    herdipo_quark3 = herdipo.Get(str(bin[i])+'_SubJet_Forward_Quark_bdt')
    herdipo_quark4 = herdipo.Get(str(bin[i])+'_SubJet_Central_Quark_bdt')

    herdipo_quark.Add(herdipo_quark2)
    herdipo_quark.Add(herdipo_quark3)
    herdipo_quark.Add(herdipo_quark4)

    pythia_gluon = pythia.Get(str(bin[i])+'_LeadingJet_Forward_Gluon_bdt')
    pythia_gluon2 = pythia.Get(str(bin[i])+'_LeadingJet_Central_Gluon_bdt')
    pythia_gluon3 = pythia.Get(str(bin[i])+'_SubJet_Forward_Gluon_bdt')
    pythia_gluon4 = pythia.Get(str(bin[i])+'_SubJet_Central_Gluon_bdt')

    pythia_gluon.Add(pythia_gluon2)
    pythia_gluon.Add(pythia_gluon3)
    pythia_gluon.Add(pythia_gluon4)

    pythia_quark = pythia.Get(str(bin[i])+'_LeadingJet_Forward_Quark_bdt')
    pythia_quark2 = pythia.Get(str(bin[i])+'_LeadingJet_Central_Quark_bdt')
    pythia_quark3 = pythia.Get(str(bin[i])+'_SubJet_Forward_Quark_bdt')
    pythia_quark4 = pythia.Get(str(bin[i])+'_SubJet_Central_Quark_bdt')

    pythia_quark.Add(pythia_quark2)
    pythia_quark.Add(pythia_quark3)
    pythia_quark.Add(pythia_quark4)

    powpyt_gluon = powpyt.Get(str(bin[i])+'_LeadingJet_Forward_Gluon_bdt')
    powpyt_gluon2 = powpyt.Get(str(bin[i])+'_LeadingJet_Central_Gluon_bdt')
    powpyt_gluon3 = powpyt.Get(str(bin[i])+'_SubJet_Forward_Gluon_bdt')
    powpyt_gluon4 = powpyt.Get(str(bin[i])+'_SubJet_Central_Gluon_bdt')

    powpyt_gluon.Add(powpyt_gluon2)
    powpyt_gluon.Add(powpyt_gluon3)
    powpyt_gluon.Add(powpyt_gluon4)

    powpyt_quark = powpyt.Get(str(bin[i])+'_LeadingJet_Forward_Quark_bdt')
    powpyt_quark2 = powpyt.Get(str(bin[i])+'_LeadingJet_Central_Quark_bdt')
    powpyt_quark3 = powpyt.Get(str(bin[i])+'_SubJet_Forward_Quark_bdt')
    powpyt_quark4 = powpyt.Get(str(bin[i])+'_SubJet_Central_Quark_bdt')

    powpyt_quark.Add(powpyt_quark2)
    powpyt_quark.Add(powpyt_quark3)
    powpyt_quark.Add(powpyt_quark4)

    me_qerr = np.zeros(powpyt_quark.GetNbinsX())
    me_gerr = np.zeros(powpyt_gluon.GetNbinsX())
    had_qerr = np.zeros(sherpa_quark.GetNbinsX())
    had_gerr = np.zeros(sherpa_quark.GetNbinsX())
    zeros = np.zeros(sherpa_quark.GetNbinsX())
    sherpa_q = np.zeros(sherpa_quark.GetNbinsX())
    sherpa_g = np.zeros(sherpa_gluon.GetNbinsX())

    for j in range(0,sherpa_quark.GetNbinsX()):
        had_qerr[j] = np.abs(scale*sherpa_quark.GetBinContent(j+1) - scale_lund*sherpa_lund_quark.GetBinContent(j+1))
        me_qerr[j] = np.abs(scale_pythia*pythia_quark.GetBinContent(j+1) - scale_powpyt*powpyt_quark.GetBinContent(j+1))

        had_gerr[j] = np.abs(scale*sherpa_gluon.GetBinContent(j+1) - scale_lund*sherpa_lund_gluon.GetBinContent(j+1))
        me_gerr[j] = np.abs(scale_pythia*pythia_gluon.GetBinContent(j+1) - scale_powpyt*powpyt_gluon.GetBinContent(j+1))

        sherpa_q[j] = scale*sherpa_quark.GetBinContent(j+1)
        sherpa_g[j] = scale*sherpa_gluon.GetBinContent(j+1)
    qmc = np.sum(sherpa_q)
    gmc = np.sum(sherpa_g)
    #print(had_qerr)
    #print(me_qerr)

    mc_unc_q = np.zeros(60)
    mc_unc_g = np.zeros(60)
    data_unc_q = np.zeros(60)
    data_unc_g = np.zeros(60)
    q = np.zeros(60)
    g = np.zeros(60)
    for j in range(0,sherpa_quark.GetNbinsX()):
        mc_unc_q[i] = np.sqrt(had_qerr[j]**2 + me_qerr[j]**2)
        mc_unc_g[i] = np.sqrt(had_gerr[j]**2 + me_gerr[j]**2)
    #print(q[10],had_q[10],me_q[10])

    #do bootstrapping bin-by-bin
    boot_q = []
    boot_g = []
    data_q = []
    data_g = []
    for j in range(0,data_quark.GetNbinsX()):
        quark_bootstrap = []
        gluon_bootstrap = []
        #print(data_quark.GetBinContent(j+1))
        for k in range(0,5000):
            quark_bootstrap.append(np.random.poisson(data_quark.GetBinContent(j+1)))
            gluon_bootstrap.append(np.random.poisson(data_gluon.GetBinContent(j+1)))

        q_stat = np.std(quark_bootstrap)
        g_stat = np.std(gluon_bootstrap)
        boot_q.append(q_stat)
        boot_g.append(g_stat)

        data_q.append(data_quark.GetBinContent(j+1))
        data_g.append(data_gluon.GetBinContent(j+1))
    qd = np.sum(data_q)
    gd = np.sum(data_g)
    qsf = qd/qmc
    gsf = gd/gmc

    print(qsf,gsf)

    histq.SetBinContent(i+1,qeff[i])
    histg.SetBinContent(i+1,grej[i])
    histqsf.SetBinContent(i+1,qsf)
    histgsf.SetBinContent(i+1,gsf)
    ##histqsf.SetBinError(i+1,qsf.std_dev)
    ##histgsf.SetBinError(i+1,gsf.std_dev)

histq.SetMaximum(1.)
histq.SetMinimum(0.)
histq.GetXaxis().SetRangeUser(500.,2000.)
histq.GetXaxis().SetTitle("Jet p_{T} (GeV)")
histq.GetYaxis().SetTitle("Efficiency")

histq.SetMarkerStyle(24)
histq.SetLineColor(4)
histq.SetMarkerColor(1)
histq.SetMarkerSize(1)

histg.SetLineColor(2)
histg.SetMarkerStyle(32)
histg.SetMarkerSize(1)
histg.SetMarkerColor(1)

histqsf.SetLineColor(4)
histqsf.SetMarkerStyle(24)
histqsf.SetMarkerSize(1)
histqsf.SetMarkerColor(1)
histqsf.SetLineStyle(7)

histgsf.SetLineColor(2)
histgsf.SetMarkerStyle(32)
histgsf.SetMarkerSize(1)
histgsf.SetMarkerColor(1)
histgsf.SetLineStyle(7)

scale = 1/rmax
histqsf.Scale(scale)
histgsf.Scale(scale)

leg = TLegend(0.6,0.25,0.84,0.55)
leg.SetTextFont(42)
leg.SetFillColor(0)
leg.SetBorderSize(0)
leg.SetFillStyle(0)
leg.AddEntry(histq,"Quark Efficiency","lp")
leg.AddEntry(histqsf,"Quark Scale Factor","lp")
leg.AddEntry(histg,"Gluon Rejection","lp")
leg.AddEntry(histgsf,"Gluon Scale Factor","lp")

histq.Draw("L P0")
histg.Draw("L P0 same")
histqsf.Draw("hist E0 L P0 same")
histgsf.Draw("hist E0 L P0 same")
qhadronization
leg.Draw("same")
myText(0.18,0.84,"#it{#bf{#scale[1.8]{#bf{ATLAS} Simulation Internal}}}")

axis = TGaxis(2000.,0.,2000.,1.,0.,rmax,510,"+L")
axis.SetTitle("Scale factor")
axis.Draw()

c.Print("test2.pdf")

