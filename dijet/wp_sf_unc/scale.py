###rescale gammajet mc and data
from ROOT import *
import numpy as np

# When quark efficiency is 0.6 ,using sherpa dijet sample as normalization ,calculating higer mc/higher data as scale factor, plotting scale factor , gluon rejection, and uncertainty. 

doreweight = 0   #decide if we want to do the reweighting process

var = "pt"  #change the var name according to the inputvar you want to read
mc = "sherpa_SF"   #by setting it as "SF" or "MC", it will automatically making scale factor plots or MC closure plots
inputvar = var  #by setting it as bdt (or ntrk,width,c1..), it will read the corresponding histogram, but remember to change the TLine range according to X-axis of different variable, one can check it by browsing the histograms in root file.

bin = [0, 50, 100, 150, 200, 300, 400, 500, 600, 800, 1000, 1200, 1500, 2000]

sherpa = TFile('../root-files/sherpa_dijet.root')
data = TFile('../root-files/dijet_data_py_wp.root')

mc_sherpa=0
data_sum = 0


for i in range(7,13):   #for only dijet event, start from jet pT>500 GeV
#for i in range(13):	#for gamma+jet combined with dijet event, start from jet pT>0 GeV
    min = bin[i]
    max = bin[i+1]
    print(i)
    #sherpa
    sherpa_quark = sherpa.Get(str(bin[i])+'_LeadingJet_Forward_Quark_pt')
    sherpa_quark2 = sherpa.Get(str(bin[i])+'_LeadingJet_Central_Quark_pt')    
    sherpa_gluon = sherpa.Get(str(bin[i])+'_LeadingJet_Forward_Gluon_pt')
    sherpa_gluon2 = sherpa.Get(str(bin[i])+'_LeadingJet_Central_Gluon_pt')
    data1 = data.Get(str(bin[i])+'_LeadingJet_Forward_Data_pt')
    data2 = data.Get(str(bin[i])+'_LeadingJet_Central_Data_pt')
    sherpa_other = sherpa.Get(str(bin[i])+'_LeadingJet_Forward_Other_pt')
    sherpa_other2 = sherpa.Get(str(bin[i])+'_LeadingJet_Central_Other_pt')

    #reassigning to variables
    sherpa_quark.Add(sherpa_quark2)
    sherpa_quark.Add(sherpa_gluon)
    sherpa_quark.Add(sherpa_gluon2)
    sherpa_quark.Add(sherpa_other)
    sherpa_quark.Add(sherpa_other2)
    data1.Add(data2)


    higher_quark = sherpa_quark.Clone("")
    higher_data = data1.Clone("")
    

    mc_sherpa += higher_quark.Integral(1,61)
    data_sum += higher_data.Integral(1,61)


print("mc_sherpa = " + str(mc_sherpa))
print("data = " + str(data_sum))

#dijet_sherpa_sys_py.root:443.3592594806105
#dijet_data_py.root:443
#
