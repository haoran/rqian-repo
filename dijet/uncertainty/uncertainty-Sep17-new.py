from ROOT import *
import numpy as np


doreweight = 0   #decide if we want to do the reweighting process

var = "bdt"  #change the var name according to the inputvar you want to read
mc = "sherpa_SF"   #by setting it as "SF" or "MC", it will automatically making scale factor plots or MC closure plots
inputvar = var  #by setting it as bdt (or ntrk,width,c1..), it will read the corresponding histogram, but remember to change the TLine range according to X-axis of different variable, one can check it by browsing the histograms in root file.

def myText(x,y,text, color = 1):
	l = TLatex()
	l.SetTextSize(0.025)
	l.SetNDC()
	l.SetTextColor(color)
	l.DrawLatex(x,y,text)
	pass

# input the mc sample(must be unnormalized)
def fraction(lower_quark,lower_gluon,higher_quark,higher_gluon):
    ToT_Fq2 = 0.
    ToT_Fg2 = 0.
    ToT_Cq2 =0.
    ToT_Cg2 = 0.

    for j in range(1,lower_quark.GetNbinsX()+1):
        ToT_Fq2+=higher_quark.GetBinContent(j)
        ToT_Cq2+=lower_quark.GetBinContent(j)
        ToT_Fg2+=higher_gluon.GetBinContent(j)
        ToT_Cg2+=lower_gluon.GetBinContent(j)

    # calculate the fraction of each sample 
    if ((ToT_Fg2+ToT_Fq2) != 0):
        fg=ToT_Fg2/(ToT_Fg2+ToT_Fq2)
        cg=ToT_Cg2/(ToT_Cq2+ToT_Cg2)

    fq=1.-fg
    cq=1.-cg
    return(fg,cg,fq,cq)


def mc_matrixmethod(lower_quark,lower_gluon,higher_quark,higher_gluon,fg,cg,fq,cq):
        
        higher = higher_quark.Clone("")
        lower = higher_quark.Clone("")
        
        for i in range(1,higher.GetNbinsX()+1):
                higher.SetBinContent(i,fg * higher_gluon.GetBinContent(i)+fq * higher_quark.GetBinContent(i))
                lower.SetBinContent(i,cg * lower_gluon.GetBinContent(i)+ cq * lower_quark.GetBinContent(i))
                pass

        #Now, let's solve.
        quark_extracted = higher_quark.Clone("")
        gluon_extracted = higher_quark.Clone("")

        #Matrix method here
        for i in range(1,higher.GetNbinsX()+1):
                F = higher.GetBinContent(i)
                C = lower.GetBinContent(i)
                if((cg*fq-fg*cq) != 0 ):
                        Q = -(C*fg-F*cg)/(cg*fq-fg*cq)
                        G = (C*fq-F*cq)/(cg*fq-fg*cq)
                        quark_extracted.SetBinContent(i,Q)
                        gluon_extracted.SetBinContent(i,G)
                        #print "   ",i,G,higher_gluon.GetBinContent(i),lower_gluon.GetBinContent(i)
                pass
            
        return(quark_extracted,gluon_extracted)
    
def percentdifference(data_quark_1,data_gluon_1,mc_quark_1,mc_gluon_1,data_quark_2,data_gluon_2,mc_quark_2,mc_gluon_2): 
    sigma_q = np.zeros(data_quark_1.GetNbinsX())
    sigma_g = np.zeros(data_quark_1.GetNbinsX())

    for j in range(1,data_quark_1.GetNbinsX()+1):
                error = 0
                value = 0
                sq = 0
                if mc_quark_1.GetBinContent(j) != 0  and mc_quark_2.GetBinContent(j) != 0 :
                    error = data_quark_1.GetBinContent(j)/mc_quark_1.GetBinContent(j) - data_quark_2.GetBinContent(j)/mc_quark_2.GetBinContent(j)
                if mc_quark_1.GetBinContent(j) != 0:
                    value = data_quark_1.GetBinContent(j)/mc_quark_1.GetBinContent(j)
                if value != 0:
                    sq = abs(error/value)
                
                error = 0
                value = 0
                sg = 0
                if mc_gluon_1.GetBinContent(j) != 0  and mc_gluon_2.GetBinContent(j) != 0 :
                    error = data_gluon_1.GetBinContent(j)/mc_gluon_1.GetBinContent(j) - data_gluon_2.GetBinContent(j)/mc_gluon_2.GetBinContent(j)
                if mc_gluon_1.GetBinContent(j) != 0:
                    value = data_gluon_1.GetBinContent(j)/mc_gluon_1.GetBinContent(j)
                if value != 0:
                    sg = abs(error/value)

                sigma_q[j-1] = sq
                sigma_g[j-1] = sg

    return sigma_q,sigma_g


def data_matrixmethod(lower_quark,lower_gluon,higher_quark,higher_gluon,higher,lower,fg,cg,fq,cq):

        #Now, let's solve.
        quark_extracted = higher_quark.Clone("")
        gluon_extracted = higher_quark.Clone("")

        #Matrix method here
        for i in range(1,higher.GetNbinsX()+1):
                F = higher.GetBinContent(i)
                C = lower.GetBinContent(i)
                if((cg*fq-fg*cq) != 0 ):
                        Q = -(C*fg-F*cg)/(cg*fq-fg*cq)
                        G = (C*fq-F*cq)/(cg*fq-fg*cq)
                        quark_extracted.SetBinContent(i,Q)
                        gluon_extracted.SetBinContent(i,G)
                        #print "   ",i,G,higher_gluon.GetBinContent(i),lower_gluon.GetBinContent(i)
                pass
            
        return(quark_extracted,gluon_extracted)
    
c1 = TCanvas("","",500,500)

bin = [0, 50, 100, 150, 200, 300, 400, 500, 600, 800, 1000, 1200, 1500, 2000]

ntrackall = TFile("../root-files/dijet_sherpa_sys_py.root")
ntrackall3 = TFile("../root-files/dijet_data_py.root")
ntrackall4 = TFile("../root-files/dijet-pythia-py.root")
ntrackall5 = TFile("../root-files/dijet-sherpa-pdf-54.root")
ntrackall6 = TFile("../root-files/dijet-sherpa-pdf-100.root")
fherdipo = TFile("../root-files/dijet_herdipo_py.root")
fherang = TFile("../root-files/dijet_herang_py.root")
sherpa_lund = TFile("../root-files/dijet_sherpalund_py.root")
powpyt = TFile("../root-files/dijet_powpyt_py.root")

for i in range(8,13):   #for only dijet event, start from jet pT>500 GeV
#for i in range(13):	#for gamma+jet combined with dijet event, start from jet pT>0 GeV
#        if(bin[i] != 800):
            c = TCanvas("c","c",500,500)
            min = bin[i]
            max = bin[i+1]
            print(min)

            higher_quark = ntrackall.Get(str(min)+"_LeadingJet_Forward_Quark_"+inputvar)
            higher_quark2 = ntrackall.Get(str(min)+"_SubJet_Forward_Quark_"+inputvar)
            higher_gluon = ntrackall.Get(str(min)+"_LeadingJet_Forward_Gluon_"+inputvar)
            higher_gluon2 = ntrackall.Get(str(min)+"_SubJet_Forward_Gluon_"+inputvar)
            lower_quark = ntrackall.Get(str(min)+"_LeadingJet_Central_Quark_"+inputvar)
            lower_quark2 = ntrackall.Get(str(min)+"_SubJet_Central_Quark_"+inputvar)
            lower_gluon = ntrackall.Get(str(min)+"_LeadingJet_Central_Gluon_"+inputvar)
            lower_gluon2 = ntrackall.Get(str(min)+"_SubJet_Central_Gluon_"+inputvar)

            higher_data = ntrackall3.Get(str(min)+"_LeadingJet_Forward_Data_"+inputvar)
            higher_data2 = ntrackall3.Get(str(min)+"_SubJet_Forward_Data_"+inputvar)
            lower_data = ntrackall3.Get(str(min)+"_LeadingJet_Central_Data_"+inputvar)
            lower_data2 = ntrackall3.Get(str(min)+"_SubJet_Central_Data_"+inputvar)

            higher_quark_pythia = ntrackall4.Get(str(min)+"_LeadingJet_Forward_Quark_"+inputvar)
            higher_quark2_pythia = ntrackall4.Get(str(min)+"_SubJet_Forward_Quark_"+inputvar)
            higher_gluon_pythia = ntrackall4.Get(str(min)+"_LeadingJet_Forward_Gluon_"+inputvar)
            higher_gluon2_pythia = ntrackall4.Get(str(min)+"_SubJet_Forward_Gluon_"+inputvar)
            lower_quark_pythia = ntrackall4.Get(str(min)+"_LeadingJet_Central_Quark_"+inputvar)
            lower_quark2_pythia = ntrackall4.Get(str(min)+"_SubJet_Central_Quark_"+inputvar)
            lower_gluon_pythia = ntrackall4.Get(str(min)+"_LeadingJet_Central_Gluon_"+inputvar)
            lower_gluon2_pythia = ntrackall4.Get(str(min)+"_SubJet_Central_Gluon_"+inputvar)

            #add leading and subleading jet from only dijet event together,
            #note that for gammajet+dijet event, we need to add leading jet from gammajet and leading jet from dijet sample together
            higher_data.Add(higher_data2)
            lower_data.Add(lower_data2)
            quark_data = higher_data.Clone("")
            gluon_data = higher_data.Clone("")
            higher_quark.Add(higher_quark2)
            higher_gluon.Add(higher_gluon2)
            lower_quark.Add(lower_quark2)
            lower_gluon.Add(lower_gluon2)

            higher_quark_pythia.Add(higher_quark2_pythia)
            higher_gluon_pythia.Add(higher_gluon2_pythia)
            lower_quark_pythia.Add(lower_quark2_pythia)
            lower_gluon_pythia.Add(lower_gluon2_pythia)
            
            higher_quark_herdipo = fherdipo.Get(str(min)+"_LeadingJet_Forward_Quark_"+inputvar)
            higher_quark2_herdipo = fherdipo.Get(str(min)+"_SubJet_Forward_Quark_"+inputvar)
            higher_gluon_herdipo = fherdipo.Get(str(min)+"_LeadingJet_Forward_Gluon_"+inputvar)
            higher_gluon2_herdipo = fherdipo.Get(str(min)+"_SubJet_Forward_Gluon_"+inputvar)
            lower_quark_herdipo = fherdipo.Get(str(min)+"_LeadingJet_Central_Quark_"+inputvar)
            lower_quark2_herdipo = fherdipo.Get(str(min)+"_SubJet_Central_Quark_"+inputvar)
            lower_gluon_herdipo = fherdipo.Get(str(min)+"_LeadingJet_Central_Gluon_"+inputvar)
            lower_gluon2_herdipo = fherdipo.Get(str(min)+"_SubJet_Central_Gluon_"+inputvar)

            higher_quark_herang = fherang.Get(str(min)+"_LeadingJet_Forward_Quark_"+inputvar)
            higher_quark2_herang = fherang.Get(str(min)+"_SubJet_Forward_Quark_"+inputvar)
            higher_gluon_herang = fherang.Get(str(min)+"_LeadingJet_Forward_Gluon_"+inputvar)
            higher_gluon2_herang = fherang.Get(str(min)+"_SubJet_Forward_Gluon_"+inputvar)
            lower_quark_herang = fherang.Get(str(min)+"_LeadingJet_Central_Quark_"+inputvar)
            lower_quark2_herang = fherang.Get(str(min)+"_SubJet_Central_Quark_"+inputvar)
            lower_gluon_herang = fherang.Get(str(min)+"_LeadingJet_Central_Gluon_"+inputvar)
            lower_gluon2_herang = fherang.Get(str(min)+"_SubJet_Central_Gluon_"+inputvar)

            higher_quark_herdipo.Add(higher_quark2_herdipo)
            higher_gluon_herdipo.Add(higher_gluon2_herdipo)
            lower_quark_herdipo.Add(lower_quark2_herdipo)
            lower_gluon_herdipo.Add(lower_gluon2_herdipo)

            higher_quark_herang.Add(higher_quark2_herang)
            higher_gluon_herang.Add(higher_gluon2_herang)
            lower_quark_herang.Add(lower_quark2_herang)
            lower_gluon_herang.Add(lower_gluon2_herang)
            
            
            hqlund = sherpa_lund.Get(str(min)+"_LeadingJet_Forward_Quark_"+inputvar)
            hqlund2 = sherpa_lund.Get(str(min)+"_SubJet_Forward_Quark_"+inputvar)
            hglund = sherpa_lund.Get(str(min)+"_LeadingJet_Forward_Gluon_"+inputvar)
            hglund2 = sherpa_lund.Get(str(min)+"_SubJet_Forward_Gluon_"+inputvar)
            lqlund = sherpa_lund.Get(str(min)+"_LeadingJet_Central_Quark_"+inputvar)
            lqlund2 = sherpa_lund.Get(str(min)+"_SubJet_Central_Quark_"+inputvar)
            lglund = sherpa_lund.Get(str(min)+"_LeadingJet_Central_Gluon_"+inputvar)
            lglund2 = sherpa_lund.Get(str(min)+"_SubJet_Central_Gluon_"+inputvar)

            hqlund.Add(hqlund2)
            hglund.Add(hglund2)
            lqlund.Add(lqlund2)
            lglund.Add(lglund2)

            higher_quark = ntrackall.Get(str(min)+"_LeadingJet_Forward_Quark_"+inputvar)
            higher_quark2 = ntrackall.Get(str(min)+"_SubJet_Forward_Quark_"+inputvar)
            higher_gluon = ntrackall.Get(str(min)+"_LeadingJet_Forward_Gluon_"+inputvar)
            higher_gluon2 = ntrackall.Get(str(min)+"_SubJet_Forward_Gluon_"+inputvar)
            lower_quark = ntrackall.Get(str(min)+"_LeadingJet_Central_Quark_"+inputvar)
            lower_quark2 = ntrackall.Get(str(min)+"_SubJet_Central_Quark_"+inputvar)
            lower_gluon = ntrackall.Get(str(min)+"_LeadingJet_Central_Gluon_"+inputvar)
            lower_gluon2 = ntrackall.Get(str(min)+"_SubJet_Central_Gluon_"+inputvar)
	    #Matrix element uncertainty: pythia - powheg+pythia
            higher_quark_pow = powpyt.Get(str(min)+"_LeadingJet_Forward_Quark_"+inputvar)
            higher_quark2_pow = powpyt.Get(str(min)+"_SubJet_Forward_Quark_"+inputvar)
            higher_gluon_pow = powpyt.Get(str(min)+"_LeadingJet_Forward_Gluon_"+inputvar)
            higher_gluon2_pow = powpyt.Get(str(min)+"_SubJet_Forward_Gluon_"+inputvar)
            lower_quark_pow = powpyt.Get(str(min)+"_LeadingJet_Central_Quark_"+inputvar)
            lower_quark2_pow = powpyt.Get(str(min)+"_SubJet_Central_Quark_"+inputvar)
            lower_gluon_pow = powpyt.Get(str(min)+"_LeadingJet_Central_Gluon_"+inputvar)
            lower_gluon2_pow = powpyt.Get(str(min)+"_SubJet_Central_Gluon_"+inputvar)

            higher_quark_pythia = ntrackall4.Get(str(min)+"_LeadingJet_Forward_Quark_"+inputvar)
            higher_quark2_pythia = ntrackall4.Get(str(min)+"_SubJet_Forward_Quark_"+inputvar)
            higher_gluon_pythia = ntrackall4.Get(str(min)+"_LeadingJet_Forward_Gluon_"+inputvar)
            higher_gluon2_pythia = ntrackall4.Get(str(min)+"_SubJet_Forward_Gluon_"+inputvar)
            lower_quark_pythia = ntrackall4.Get(str(min)+"_LeadingJet_Central_Quark_"+inputvar)
            lower_quark2_pythia = ntrackall4.Get(str(min)+"_SubJet_Central_Quark_"+inputvar)
            lower_gluon_pythia = ntrackall4.Get(str(min)+"_LeadingJet_Central_Gluon_"+inputvar)
            lower_gluon2_pythia = ntrackall4.Get(str(min)+"_SubJet_Central_Gluon_"+inputvar)

            higher_quark_pow.Add(higher_quark2_pow)
            higher_gluon_pow.Add(higher_gluon2_pow)
            lower_quark_pow.Add(lower_quark2_pow)
            lower_gluon_pow.Add(lower_gluon2_pow)

            higher_quark_pythia.Add(higher_quark2_pythia)
            higher_gluon_pythia.Add(higher_gluon2_pythia)
            lower_quark_pythia.Add(lower_quark2_pythia)
            lower_gluon_pythia.Add(lower_gluon2_pythia)
            
            
            #uncertainty calculations
            #uncertainty lists, number-of-bins lists of 4 uncertainties.
            sigma_tot_q = []
            sigma_tot_g = []
            ebar_q = []
            ebar_g = []

            for j in range(0,quark_data.GetNbinsX()):
                    sigma_tot_q += [np.zeros(8)]
                    sigma_tot_g += [np.zeros(8)]


        
            # do matrix method to extract the distribution of sherpa first
            
            fg,cg,fq,cq = fraction(lower_quark,lower_gluon,higher_quark,higher_gluon)

            # normalize the sherpa mc
            if (lower_quark.Integral() != 0):
                lower_quark.Scale(1./lower_quark.Integral())
            if(lower_gluon.Integral() != 0):
                lower_gluon.Scale(1./lower_gluon.Integral())
            if(higher_quark.Integral() != 0):
                higher_quark.Scale(1./higher_quark.Integral())
            if(higher_gluon.Integral() != 0):
                higher_gluon.Scale(1./higher_gluon.Integral())            
                        
            sherpa_extract_Q,sherpa_extract_G = mc_matrixmethod(lower_quark,lower_gluon,higher_quark,higher_gluon,fg,cg,fq,cq)

            #statistical
            # do bootstrap(not normalized yet)
            # create lists to store bootstrapped values list of arrays of nstraps values
            nstraps = 5000
            SFQvals = []
            SFGvals = []

            for j in range(1,quark_data.GetNbinsX()+1):
                    SFQvals += [np.zeros(nstraps)]
                    SFGvals += [np.zeros(nstraps)]
                    
            for k in range(nstraps):

                    forward_data_strap = higher_data.Clone("f"+str(k))
                    central_data_strap = lower_data.Clone("c"+str(k))

                    for j in range(1,higher_quark.GetNbinsX()+1):
                            forward_data_strap.SetBinContent(j,np.random.poisson(higher_data.GetBinContent(j)))
                            central_data_strap.SetBinContent(j,np.random.poisson(lower_data.GetBinContent(j)))
                            
                    if forward_data_strap.Integral() != 0:
                        forward_data_strap.Scale(1/forward_data_strap.Integral())
                    if central_data_strap.Integral() != 0:
                        central_data_strap.Scale(1/central_data_strap.Integral())                            
                            
                            
                    # get extracted data Q/G with sherpa sample
                    for j in range(0,higher_quark.GetNbinsX()):
                            F_data = forward_data_strap.GetBinContent(j)
                            C_data = central_data_strap.GetBinContent(j)
                            Q_data = -(C_data*fg-F_data*cg)/(cg*fq-fg*cq)
                            G_data = (C_data*fq-F_data*cq)/(cg*fq-fg*cq)

                            SFQvals[j][k] = Q_data/sherpa_extract_Q.GetBinContent(i+1)
                            SFGvals[j][k] = G_data/sherpa_extract_G.GetBinContent(i+1)
                    

            
            #compute the uncertainty and plots
            quark_strap = quark_data.Clone("")
            gluon_strap = gluon_data.Clone("")

            
            #do matrix method on data (sherpa fraction)
            # first normalize it
            if (higher_data.Integral() != 0):
                higher_data.Scale(1/higher_data.Integral())
            if (lower_data.Integral() != 0):
                lower_data.Scale(1/lower_data.Integral())
                
            extracted_data_sherpa_Q,extracted_data_sherpa_G = data_matrixmethod(lower_quark,lower_gluon,higher_quark,higher_gluon,higher_data,lower_data,fg,cg,fq,cq)
            

            
                
                
                
            for j in range(0,quark_data.GetNbinsX()):
                    SFQvals[j].sort()
                    SFGvals[j].sort()
                    SFQ = np.median(SFQvals[j])
                    SFG = np.median(SFGvals[j])
                    
                    sigmaSFQ = .5*(SFQvals[j][int(.84*len(SFQvals[j]))] - SFQvals[j][int(.16*len(SFQvals[j]))])
                    sigmaSFG = .5*(SFGvals[j][int(.84*len(SFGvals[j]))] - SFGvals[j][int(.16*len(SFGvals[j]))])
		    #ebar_q[j][0] = sigmaQ/np.sum(sigmaQ[j])
		    #ebar_g[j][0] = sigmaG/np.sum(sigmaG[j])

                    #print("statistical: q = "+str(sigmaQ)+" | g = "+str(sigmaG))                  
                    
                    if(SFQ != 0):
                            sigmaSFQ = np.abs(sigmaSFQ/SFQ)
                    else:
                            sigmaSFQ = 0
                            
                    if(SFG != 0):
                            sigmaSFG = np.abs(sigmaSFG/SFG)
                    else:
                            sigmaSFG = 0
                            
                    sigma_tot_q[j][0] = sigmaSFQ
                    sigma_tot_g[j][0] = sigmaSFG

                    quark_strap.SetBinContent(j+1,sigmaSFQ)
                    gluon_strap.SetBinContent(j+1,sigmaSFG)

            quark_strap_negative = quark_strap.Clone("")
            gluon_strap_negative = gluon_strap.Clone("")

            quark_strap_negative = quark_strap_negative * -1
            gluon_strap_negative = gluon_strap_negative * -1
            #print(extracted_data_sherpa_Q.Integral(),sherpa_extract_Q.Integral())
            
            # mc closure
            #mc uncertainty
            #uncertainty calculation percent difference
            
            sigma_q,sigma_g = percentdifference(extracted_data_sherpa_Q,extracted_data_sherpa_G,sherpa_extract_Q,sherpa_extract_G,extracted_data_sherpa_Q,extracted_data_sherpa_G,higher_quark,higher_gluon)
            quark_use = higher_quark.Clone()
            gluon_use = higher_gluon.Clone()

            for j in range(1,higher_quark.GetNbinsX()+1):
                    quark_use.SetBinContent(j,sigma_q[j-1])
                    gluon_use.SetBinContent(j,sigma_g[j-1])

                    sigma_tot_q[j-1][1] = sigma_q[j-1]
                    sigma_tot_g[j-1][1] = sigma_g[j-1]

            quarkMC_negative = quark_use.Clone()
            gluonMC_negative = gluon_use.Clone()

            quarkMC_negative.Scale(-1)
            gluonMC_negative.Scale(-1)
            
            
            #pdf uncertainty. stdev of binvals

            pdf_qvals = []
            pdf_gvals = []

            for i in range(1,higher_quark.GetNbinsX()+1):
                    pdf_qvals += [np.zeros(100)]
                    pdf_gvals += [np.zeros(100)]

            for k in range(1,101):
                    if(k < 55):
                            higher_quark_pdf = ntrackall5.Get(str(min)+"_LeadingJet_Forward_Quark"+str(k)+"_"+inputvar)
                            higher_quark1_pdf = ntrackall5.Get(str(min)+"_SubJet_Forward_Quark"+str(k)+"_"+inputvar)
                            lower_quark_pdf = ntrackall5.Get(str(min)+"_LeadingJet_Central_Quark"+str(k)+"_"+inputvar)
                            lower_quark1_pdf = ntrackall5.Get(str(min)+"_SubJet_Central_Quark"+str(k)+"_"+inputvar)
                            higher_gluon_pdf = ntrackall5.Get(str(min)+"_LeadingJet_Forward_Quark"+str(k)+"_"+inputvar)
                            higher_gluon1_pdf = ntrackall5.Get(str(min)+"_LeadingJet_Forward_Quark"+str(k)+"_"+inputvar)
                            lower_gluon_pdf = ntrackall5.Get(str(min)+"_LeadingJet_Forward_Quark"+str(k)+"_"+inputvar)
                            lower_gluon1_pdf = ntrackall5.Get(str(min)+"_LeadingJet_Forward_Quark"+str(k)+"_"+inputvar)
                    else:
                            higher_quark_pdf = ntrackall6.Get(str(min)+"_LeadingJet_Forward_Quark"+str(k)+"_"+inputvar)
                            higher_quark1_pdf = ntrackall6.Get(str(min)+"_SubJet_Forward_Quark"+str(k)+"_"+inputvar)
                            lower_quark_pdf = ntrackall6.Get(str(min)+"_LeadingJet_Central_Quark"+str(k)+"_"+inputvar)
                            lower_quark1_pdf = ntrackall6.Get(str(min)+"_SubJet_Central_Quark"+str(k)+"_"+inputvar)
                            higher_gluon_pdf = ntrackall6.Get(str(min)+"_LeadingJet_Forward_Quark"+str(k)+"_"+inputvar)
                            higher_gluon1_pdf = ntrackall6.Get(str(min)+"_LeadingJet_Forward_Quark"+str(k)+"_"+inputvar)
                            lower_gluon_pdf = ntrackall6.Get(str(min)+"_LeadingJet_Forward_Quark"+str(k)+"_"+inputvar)
                            lower_gluon1_pdf = ntrackall6.Get(str(min)+"_LeadingJet_Forward_Quark"+str(k)+"_"+inputvar)

                    higher_quark_pdf.Add(higher_quark1_pdf)
                    higher_gluon_pdf.Add(higher_gluon1_pdf)
                    lower_quark_pdf.Add(lower_quark1_pdf)
                    lower_gluon_pdf.Add(lower_gluon1_pdf)

                    ToT_Fq2 = 0.
                    ToT_Fg2 = 0.
        
                    ToT_Cq2 = 0.
                    ToT_Cg2 = 0.
        
                    for j in range(1,lower_quark_pdf.GetNbinsX()+1):
                            #print(j,higher_gluon_pdf.GetBinContent(j))

                            ToT_Fq2+=higher_quark_pdf.GetBinContent(j)
                            ToT_Cq2+=lower_quark_pdf.GetBinContent(j)
                            ToT_Fg2+=higher_gluon_pdf.GetBinContent(j)
                            ToT_Cg2+=lower_gluon_pdf.GetBinContent(j)
        
                    fg_pdf=ToT_Fg2/(ToT_Fg2+ToT_Fq2)
                    cg_pdf=ToT_Cg2/(ToT_Cq2+ToT_Cg2)
                    fq_pdf=1.-fg
                    cq_pdf=1.-cg
                    
                    if(lower_quark_pdf.Integral() != 0):
                            lower_quark_pdf.Scale(1./lower_quark_pdf.Integral())
                    if(lower_gluon_pdf.Integral() != 0):
                            lower_gluon_pdf.Scale(1./lower_gluon_pdf.Integral())
                    if(higher_quark_pdf.Integral() != 0):
                            higher_quark_pdf.Scale(1./higher_quark_pdf.Integral())
                    if(higher_gluon_pdf.Integral() != 0):
                            higher_gluon_pdf.Scale(1./higher_gluon_pdf.Integral())
                            

                    higher_pdf = higher_quark_pdf.Clone("")
                    lower_pdf = higher_quark_pdf.Clone("")

                    for i in range(1,higher_quark.GetNbinsX()+1):
                            higher_pdf.SetBinContent(i,fg_pdf*higher_gluon_pdf.GetBinContent(i)+fq_pdf*higher_quark_pdf.GetBinContent(i))
                            lower_pdf.SetBinContent(i,cg_pdf*lower_gluon_pdf.GetBinContent(i)+cq_pdf*lower_quark_pdf.GetBinContent(i))
                            pass
                    #for i in range(1,higher_quark.GetNbinsX()+1):
                            #print(higher_pdf.GetBinContent(i))
                    pdf_extract_Q,pdf_extract_G = mc_matrixmethod(lower_quark_pdf,lower_gluon_pdf,higher_quark_pdf,higher_gluon_pdf,fg_pdf,cg_pdf,fq_pdf,cq_pdf)
                    extracted_data_pdf_Q,extracted_data_pdf_G = data_matrixmethod(lower_quark_pdf,lower_gluon_pdf,higher_quark_pdf,higher_gluon_pdf,higher_data,lower_data,fg_pdf,cg_pdf,fq_pdf,cq_pdf)
        
                    #Now, let's solve
                    #Matrix method here
                    for i in range(1,higher_quark.GetNbinsX()+1):
                                    if extracted_data_pdf_Q.GetBinContent(i)!= 0 :
                                        pdf_qvals[i-1][k-1] = pdf_extract_Q.GetBinContent(i)/extracted_data_pdf_Q.GetBinContent(i)
                                    if extracted_data_pdf_G.GetBinContent(i)!= 0 :
                                        pdf_gvals[i-1][k-1] = pdf_extract_G.GetBinContent(i)/extracted_data_pdf_G.GetBinContent(i)

            quark_pdf = higher_quark.Clone("")
            gluon_pdf = higher_quark.Clone("")

            for j in range(0,higher_quark.GetNbinsX()):
                    pdf_qvals[j].sort()
                    pdf_gvals[j].sort()
                    Q = np.median(pdf_qvals[j])
                    G = np.median(pdf_gvals[j])
                     
                    pdf_sigmaQ = np.std(pdf_qvals[j])
                    pdf_sigmaG = np.std(pdf_gvals[j])
                    #print(j,Q,pdf_sigmaQ)
                    #print("PDF: q = "+str(pdf_sigmaQ)+" | g = "+str(pdf_sigmaG))
                    #print(j,pdf_extract_Q.GetBinContent(j),extracted_data_pdf_Q.GetBinContent(j))
                    if(Q != 0):
                            pdf_sigmaQ = np.abs(pdf_sigmaQ/Q)
                    if(G != 0):
                            pdf_sigmaG = np.abs(pdf_sigmaG/G)

                    sigma_tot_q[j][3] = pdf_sigmaQ
                    sigma_tot_g[j][3] = pdf_sigmaG

                    quark_pdf.SetBinContent(j+1,pdf_sigmaQ)
                    gluon_pdf.SetBinContent(j+1,pdf_sigmaG)

            quark_pdf_negative = quark_pdf.Clone("")
            gluon_pdf_negative = gluon_pdf.Clone("")

            quark_pdf_negative = quark_pdf_negative * -1
            gluon_pdf_negative = gluon_pdf_negative * -1
            
            #new showering is difference in herwing angular and dipole
            
            # normalize the herdipo and herang mc
            fg_herang,cg_herang,fq_herang,cq_herang = fraction(lower_quark_herang,lower_gluon_herang,higher_quark_herang,higher_gluon_herang)
            fg_herdipo,cg_herdipo,fq_herdipo,cq_herdipo = fraction(lower_quark_herdipo,lower_gluon_herdipo,higher_quark_herdipo,higher_gluon_herdipo)


            if (lower_quark_herang.Integral() != 0):
                lower_quark_herang.Scale(1./lower_quark_herang.Integral())
            if(lower_gluon_herang.Integral() != 0):
                lower_gluon_herang.Scale(1./lower_gluon_herang.Integral())
            if(higher_quark_herang.Integral() != 0):
                higher_quark_herang.Scale(1./higher_quark_herang.Integral())
            if(higher_gluon_herang.Integral() != 0):
                higher_gluon_herang.Scale(1./higher_gluon_herang.Integral())    
                
            if (lower_quark_herdipo.Integral() != 0):
                lower_quark_herdipo.Scale(1./lower_quark_herdipo.Integral())
            if(lower_gluon_herdipo.Integral() != 0):
                lower_gluon_herdipo.Scale(1./lower_gluon_herdipo.Integral())
            if(higher_quark_herdipo.Integral() != 0):
                higher_quark_herdipo.Scale(1./higher_quark_herdipo.Integral())
            if(higher_gluon_herdipo.Integral() != 0):
                higher_gluon_herdipo.Scale(1./higher_gluon_herdipo.Integral())              

            herang_extract_Q,herang_extract_G = mc_matrixmethod(lower_quark_herang,lower_gluon_herang,higher_quark_herang,higher_gluon_herang,fg_herang,cg_herang,fq_herang,cq_herang)
            extracted_data_herang_Q,extracted_data_herang_G = data_matrixmethod(lower_quark_herang,lower_gluon_herang,higher_quark_herang,higher_gluon_herang,higher_data,lower_data,fg_herang,cg_herang,fq_herang,cq_herang)
            herdipo_extract_Q,herdipo_extract_G = mc_matrixmethod(lower_quark_herdipo,lower_gluon_herdipo,higher_quark_herdipo,higher_gluon_herdipo,fg_herdipo,cg_herdipo,fq_herdipo,cq_herdipo)
            extracted_data_herdipo_Q,extracted_data_herdipo_G = data_matrixmethod(lower_quark_herdipo,lower_gluon_herdipo,higher_quark_herdipo,higher_gluon_herdipo,higher_data,lower_data,fg_herdipo,cg_herdipo,fq_herdipo,cq_herdipo)
            
            q_show_unc = higher_quark_herdipo.Clone()
            g_show_unc = higher_gluon_herdipo.Clone()
            qshow,gshow = percentdifference(extracted_data_herang_Q,extracted_data_herang_G,herang_extract_Q,herang_extract_G,extracted_data_herdipo_Q,extracted_data_herdipo_G,herdipo_extract_Q,herdipo_extract_G)

            for j in range(1,higher_quark_herdipo.GetNbinsX()+1):
                    q_show_unc.SetBinContent(j,qshow[j-1])
                    g_show_unc.SetBinContent(j,gshow[j-1])

                    sigma_tot_q[j-1][4] = qshow[j-1]
                    sigma_tot_g[j-1][4] = gshow[j-1]

            q_show_uncn = q_show_unc.Clone()
            g_show_uncn = g_show_unc.Clone()

            q_show_uncn.Scale(-1)
            g_show_uncn.Scale(-1)

            #hadronization, difference in sherpa
            fg_lund,cg_lund,fq_lund,cq_lund = fraction(lqlund,lglund,hqlund,hglund)

            if (hqlund.Integral() != 0):
                hqlund.Scale(1./hqlund.Integral())
            if(hglund.Integral() != 0):
                hglund.Scale(1./hglund.Integral())
            if(lqlund.Integral() != 0):
                lqlund.Scale(1./lqlund.Integral())
            if(lglund.Integral() != 0):
                lglund.Scale(1./lglund.Integral())
                
            lower_quark_lund =  lqlund.Clone("")
            lower_gluon_lund =  lglund.Clone("")
            higher_quark_lund =  hqlund.Clone("")
            higher_gluon_lund =  hglund.Clone("")
            
            lund_extract_Q,lund_extract_G = mc_matrixmethod(lower_quark_lund,lower_gluon_lund,higher_quark_lund,higher_gluon_lund,fg_lund,cg_lund,fq_lund,cq_lund)
            extracted_data_lund_Q,extracted_data_lund_G = data_matrixmethod(lower_quark_lund,lower_gluon_lund,higher_quark_lund,higher_gluon_lund,higher_data,lower_data,fg_lund,cg_lund,fq_lund,cq_lund)

            qhadunc = hqlund.Clone()
            ghadunc = hqlund.Clone()
            qhad,ghad = percentdifference(extracted_data_sherpa_Q,extracted_data_sherpa_G,sherpa_extract_Q,sherpa_extract_G,extracted_data_lund_Q,extracted_data_lund_G,lund_extract_Q,lund_extract_G)

            for j in range(1,hqlund.GetNbinsX()+1):
                    qhadunc.SetBinContent(j,qhad[j-1])
                    ghadunc.SetBinContent(j,ghad[j-1])
                    sigma_tot_q[j-1][5] = qhad[j-1]
                    sigma_tot_g[j-1][5] = ghad[j-1]

            qhadn = qhadunc.Clone()
            ghadn = ghadunc.Clone()

            qhadn.Scale(-1)
            ghadn.Scale(-1)


	        #Matrix element uncertainty: pythia - powheg+pythia
            fg_pythia,cg_pythia,fq_pythia,cq_pythia = fraction(lower_quark_pythia,lower_gluon_pythia,higher_quark_pythia,higher_gluon_pythia)
            fg_pow,cg_pow,fq_pow,cq_pow = fraction(lower_quark_pow,lower_gluon_pow,higher_quark_pow,higher_gluon_pow)


            if (lower_quark_pythia.Integral() != 0):
                lower_quark_pythia.Scale(1./lower_quark_pythia.Integral())
            if(lower_gluon_pythia.Integral() != 0):
                lower_gluon_pythia.Scale(1./lower_gluon_pythia.Integral())
            if(higher_quark_pythia.Integral() != 0):
                higher_quark_pythia.Scale(1./higher_quark_pythia.Integral())
            if(higher_gluon_pythia.Integral() != 0):
                higher_gluon_pythia.Scale(1./higher_gluon_pythia.Integral())    
                
            if (lower_quark_pow.Integral() != 0):
                lower_quark_pow.Scale(1./lower_quark_pow.Integral())
            if(lower_gluon_pow.Integral() != 0):
                lower_gluon_pow.Scale(1./lower_gluon_pow.Integral())
            if(higher_quark_pow.Integral() != 0):
                higher_quark_pow.Scale(1./higher_quark_pow.Integral())
            if(higher_gluon_pow.Integral() != 0):
                higher_gluon_pow.Scale(1./higher_gluon_pow.Integral())              

            pythia_extract_Q,pythia_extract_G = mc_matrixmethod(lower_quark_pythia,lower_gluon_pythia,higher_quark_pythia,higher_gluon_pythia,fg_pythia,cg_pythia,fq_pythia,cq_pythia)
            extracted_data_pythia_Q,extracted_data_pythia_G = data_matrixmethod(lower_quark_pythia,lower_gluon_pythia,higher_quark_pythia,higher_gluon_pythia,higher_data,lower_data,fg_pythia,cg_pythia,fq_pythia,cq_pythia)
            pow_extract_Q,pow_extract_G = mc_matrixmethod(lower_quark_pow,lower_gluon_pow,higher_quark_pow,higher_gluon_pow,fg_pow,cg_pow,fq_pow,cq_pow)
            extracted_data_pow_Q,extracted_data_pow_G = data_matrixmethod(lower_quark_pow,lower_gluon_pow,higher_quark_pow,higher_gluon_pow,higher_data,lower_data,fg_pow,cg_pow,fq_pow,cq_pow)

            
            qmeunc = higher_quark_pow.Clone()
            gmeunc = higher_gluon_pow.Clone()
            qme,gme = percentdifference(extracted_data_pythia_Q,extracted_data_pythia_G,pythia_extract_Q,pythia_extract_G,extracted_data_pow_Q,extracted_data_pow_G,pow_extract_Q,pow_extract_G)

            for j in range(1,higher_quark_pow.GetNbinsX()+1):
                    qmeunc.SetBinContent(j,qme[j-1])
                    gmeunc.SetBinContent(j,gme[j-1])

                    sigma_tot_q[j-1][6] = qme[j-1]
                    sigma_tot_g[j-1][6] = gme[j-1]

            qmen = qmeunc.Clone()
            gmen = gmeunc.Clone()

            qmen.Scale(-1)
            gmen.Scale(-1)
            ### scale variation ### (currently only test) using powpyt
            higher_quark_pow = powpyt.Get(str(min)+"_LeadingJet_Forward_Quark_"+inputvar)
            higher_quark2_pow = powpyt.Get(str(min)+"_SubJet_Forward_Quark_"+inputvar)
            higher_gluon_pow = powpyt.Get(str(min)+"_LeadingJet_Forward_Gluon_"+inputvar)
            higher_gluon2_pow = powpyt.Get(str(min)+"_SubJet_Forward_Gluon_"+inputvar)
            lower_quark_pow = powpyt.Get(str(min)+"_LeadingJet_Central_Quark_"+inputvar)
            lower_quark2_pow = powpyt.Get(str(min)+"_SubJet_Central_Quark_"+inputvar)
            lower_gluon_pow = powpyt.Get(str(min)+"_LeadingJet_Central_Gluon_"+inputvar)
            lower_gluon2_pow = powpyt.Get(str(min)+"_SubJet_Central_Gluon_"+inputvar)

            higher_quark_pow.Add(higher_quark2_pow)
            higher_gluon_pow.Add(higher_gluon2_pow)
            lower_quark_pow.Add(lower_quark2_pow)
            lower_gluon_pow.Add(lower_gluon2_pow)
         
            if (lower_quark_pow.Integral() != 0):
                lower_quark_pow.Scale(1./lower_quark_pow.Integral())
            if(lower_gluon_pow.Integral() != 0):
                lower_gluon_pow.Scale(1./lower_gluon_pow.Integral())
            if(higher_quark_pow.Integral() != 0):
                higher_quark_pow.Scale(1./higher_quark_pow.Integral())
            if(higher_gluon_pow.Integral() != 0):
                higher_gluon_pow.Scale(1./higher_gluon_pow.Integral())              

            pow_extract_Q,pow_extract_G = mc_matrixmethod(lower_quark_pow,lower_gluon_pow,higher_quark_pow,higher_gluon_pow,fg_pow,cg_pow,fq_pow,cq_pow)
            extracted_data_pow_Q,extracted_data_pow_G = data_matrixmethod(lower_quark_pow,lower_gluon_pow,higher_quark_pow,higher_gluon_pow,higher_data,lower_data,fg_pow,cg_pow,fq_pow,cq_pow)

            for k in range(1,7):
                powpyt1 = TFile("../root-files/sherpa_sv/dijet_powpyt_py_"+str(k)+".root")
                higher_quark_SV = powpyt1.Get(str(min)+"_LeadingJet_Forward_Quark_"+inputvar)
                higher_quark2_SV = powpyt1.Get(str(min)+"_SubJet_Forward_Quark_"+inputvar)
                higher_gluon_SV = powpyt1.Get(str(min)+"_LeadingJet_Forward_Gluon_"+inputvar)
                higher_gluon2_SV = powpyt1.Get(str(min)+"_SubJet_Forward_Gluon_"+inputvar)
                lower_quark_SV = powpyt1.Get(str(min)+"_LeadingJet_Central_Quark_"+inputvar)
                lower_quark2_SV = powpyt1.Get(str(min)+"_SubJet_Central_Quark_"+inputvar)
                lower_gluon_SV = powpyt1.Get(str(min)+"_LeadingJet_Central_Gluon_"+inputvar)
                lower_gluon2_SV = powpyt1.Get(str(min)+"_SubJet_Central_Gluon_"+inputvar)
                
                higher_quark_SV.Add(higher_quark2_SV)
                higher_gluon_SV.Add(higher_gluon2_SV)
                lower_quark_SV.Add(lower_quark2_SV)
                lower_gluon_SV.Add(lower_gluon2_SV)

                fg_SV,cg_SV,fq_SV,cq_SV = fraction(lower_quark_SV,lower_gluon_SV,higher_quark_SV,higher_gluon_SV)


                if (lower_quark_SV.Integral() != 0):
                    lower_quark_SV.Scale(1./lower_quark_SV.Integral())
                if(lower_gluon_SV.Integral() != 0):
                    lower_gluon_SV.Scale(1./lower_gluon_SV.Integral())
                if(higher_quark_SV.Integral() != 0):
                    higher_quark_SV.Scale(1./higher_quark_SV.Integral())
                if(higher_gluon_SV.Integral() != 0):
                    higher_gluon_SV.Scale(1./higher_gluon_SV.Integral())    

                SV_extract_Q,SV_extract_G = mc_matrixmethod(lower_quark_SV,lower_gluon_SV,higher_quark_SV,higher_gluon_SV,fg_SV,cg_SV,fq_SV,cq_SV)
                extracted_data_SV_Q,extracted_data_SV_G = data_matrixmethod(lower_quark_SV,lower_gluon_SV,higher_quark_SV,higher_gluon_SV,higher_data,lower_data,fg_SV,cg_SV,fq_SV,cq_SV)
                #for i in range(1,61):
                    #print(i,extracted_data_pow_Q.GetBinContent(i))
                if k == 1:
                    prevq,prevg = percentdifference(extracted_data_pow_Q,extracted_data_pow_G,pow_extract_Q,pow_extract_G,extracted_data_SV_Q,extracted_data_SV_G,SV_extract_Q,SV_extract_G)

                if k > 1:
                    diffq,diffg = percentdifference(extracted_data_pow_Q,extracted_data_pow_G,pow_extract_Q,pow_extract_G,extracted_data_SV_Q,extracted_data_SV_G,SV_extract_Q,SV_extract_G)

                    for l in range(0,higher_quark_SV.GetNbinsX()):
                        if(diffq[l]>prevq[l]):
                            prevq = diffq
                        if(diffg[l]>prevg[l]):
                            prevg = diffg
    

            svunq1,svung1 = prevq,prevg
            
            qsvunc = higher_quark_SV.Clone()
            gsvunc = higher_quark_SV.Clone()

            for j in range(1,higher_quark_SV.GetNbinsX()+1):
                    qsvunc.SetBinContent(j,svunq1[j-1])
                    gsvunc.SetBinContent(j,svung1[j-1])

                    sigma_tot_q[j-1][7] = svunq1[j-1]
                    sigma_tot_g[j-1][7] = svung1[j-1]

            qsvn = qsvunc.Clone()
            gsvn = gsvunc.Clone()

            qsvn.Scale(-1)
            gsvn.Scale(-1) 
            #total uncertainty
            q_sigma_tot = higher_quark.Clone("")
            g_sigma_tot = higher_quark.Clone("")

            for j in range (0, higher_quark.GetNbinsX()):
                    #print(sigma_tot_q[j][5],sigma_tot_g[j][5])
                    a = sigma_tot_q[j][0]
                    b = sigma_tot_q[j][1]
                    #c = sigma_tot_q[j][2]
                    d = sigma_tot_q[j][3]
                    e = sigma_tot_q[j][4]
                    f = sigma_tot_q[j][5]
                    g = sigma_tot_q[j][6]
                    h = sigma_tot_q[j][7]
                    #print(a,b,c,d,e,f,g,h)
                    sigma_q_tot = np.sqrt((a**2)+(b**2)+(d**2)+(e**2)+(f**2)+(g**2)+(h**2))


                    a = sigma_tot_g[j][0]
                    b = sigma_tot_g[j][1]
                    #c = sigma_tot_g[j][2]
                    d = sigma_tot_g[j][3]
                    e = sigma_tot_g[j][4]
                    f = sigma_tot_g[j][5]
                    g = sigma_tot_g[j][6]
                    h = sigma_tot_g[j][7]
                    sigma_g_tot = np.sqrt((a**2)+(b**2)+(d**2)+(e**2)+(f**2)+(g**2)+(h**2))


                    q_sigma_tot.SetBinContent(j+1,sigma_q_tot)
                    g_sigma_tot.SetBinContent(j+1,sigma_g_tot)

                    #print("statistical: "+str(100*a)+" , MC Closure: "+str(100*b)+" , Showering: "+str(100*c)+" , PDF: "+str(100*d))

            q_sigma_tot.Scale(100)
            g_sigma_tot.Scale(100)

            q_sigma_tot_n = q_sigma_tot.Clone("")
            g_sigma_tot_n = g_sigma_tot.Clone("")
            q_sigma_tot_n.Scale(-1)
            g_sigma_tot_n.Scale(-1)

            quark_pdf.Scale(100)
            gluon_pdf.Scale(100)
            quark_pdf_negative.Scale(100)
            gluon_pdf_negative.Scale(100)

            quark_strap.Scale(100)
            gluon_strap.Scale(100)
            quark_strap_negative.Scale(100)
            gluon_strap_negative.Scale(100)

            quark_use.Scale(100)
            gluon_use.Scale(100)
            quarkMC_negative.Scale(100)
            gluonMC_negative.Scale(100)

            q_show_unc.Scale(100)
            g_show_unc.Scale(100)
            q_show_uncn.Scale(100)
            g_show_uncn.Scale(100)

            qhadunc.Scale(100)
            ghadunc.Scale(100)
            qhadn.Scale(100)
            ghadn.Scale(100)

            qmeunc.Scale(100)
            gmeunc.Scale(100)
            qmen.Scale(100)
            gmen.Scale(100)

            qsvunc.Scale(100)
            gsvunc.Scale(100)
            qsvn.Scale(100)
            gsvn.Scale(100)

            ## below just do the ploting
            gPad.SetLeftMargin(0.15)
            gPad.SetTopMargin(0.05)
            gPad.SetBottomMargin(0.15)
            gPad.SetRightMargin(0.2)



            gStyle.SetOptStat(0)
            ######################## for ratio plo

            quark_strap.GetYaxis().SetRangeUser(-50,50)
            quark_strap.SetLineColor(2)
            quark_strap.SetLineStyle(2)
            #quark_strap.SetMarkerColor(8)
            #quark_strap.SetMarkerSize(0.8)
            quark_strap_negative.SetLineColor(2)
            quark_strap_negative.SetLineStyle(2)
            #quark_strap_negative.SetMarkerSize(0.8)
            #quark_strap_negative.SetMarkerColor(8)

            quark_use.SetLineColor(30)
            quark_use.SetLineStyle(2)
            #quark_use.SetMarkerColor(2)
            #quark_use.SetMarkerSize(0.8)
            quarkMC_negative.SetLineColor(30)
            quarkMC_negative.SetLineStyle(2)
            #quarkMC_negative.SetMarkerColor(2)
            #quarkMC_negative.SetMarkerSize(0.8)


            quark_pdf.SetLineColor(28)
            quark_pdf.SetLineStyle(2)
            quark_pdf_negative.SetLineColor(28)
            quark_pdf_negative.SetLineStyle(2)

            q_show_unc.SetLineColor(1)
            q_show_unc.SetLineStyle(2)
            q_show_uncn.SetLineColor(1)
            q_show_uncn.SetLineStyle(2)

            qhadunc.SetLineColor(9)
            qhadunc.SetLineStyle(2)
            qhadn.SetLineColor(9)
            qhadn.SetLineStyle(2)

            qmeunc.SetLineColor(7)
            qmeunc.SetLineStyle(2)
            qmen.SetLineColor(7)
            qmen.SetLineStyle(2)

            qsvunc.SetLineColor(6)
            qsvunc.SetLineStyle(2)
            qsvn.SetLineColor(6)
            qsvn.SetLineStyle(2)

            q_sigma_tot.SetLineColor(4)
            q_sigma_tot.SetLineStyle(1)
            q_sigma_tot.SetLineWidth(2)
            q_sigma_tot_n.SetLineColor(4)
            q_sigma_tot_n.SetLineStyle(1)
            q_sigma_tot_n.SetLineWidth(2)

            quark_strap.GetYaxis().SetTitle("Uncertainty (%)")

            quark_strap.Draw("HIST")
            quark_strap_negative.Draw("HIST same")
            quark_use.Draw("HIST same")
            quarkMC_negative.Draw("HIST same")
            quark_pdf.Draw("HIST same")
            quark_pdf_negative.Draw("HIST same")
            q_sigma_tot.Draw("HIST same")
            q_sigma_tot_n.Draw("HIST same")
            q_show_unc.Draw("HIST same")
            q_show_uncn.Draw("HIST same")
            qhadunc.Draw("hist same")
            qhadn.Draw("hist same")
            qmeunc.Draw("hist same")
            qmen.Draw("hist same")
            qsvunc.Draw("hist same")
            qsvn.Draw("hist same")

            leg = TLegend(0.82,0.7,0.98,0.9) ##0.6,0.5,0.9,0.7
            leg.SetTextFont(42)
            leg.SetFillColor(0)
            leg.SetBorderSize(0)
            leg.SetFillStyle(0)
            leg.SetNColumns(1)
            leg.AddEntry(quark_strap,"Statistical","l")
            leg.AddEntry(quark_use,"MC Closure","l")
            leg.AddEntry(quark_pdf,"PDF","l")
            leg.AddEntry(q_show_unc,"Showering","l")
            leg.AddEntry(qhadunc,"Hadronization","l")
            leg.AddEntry(qmeunc,"Matrix Element","l")
            leg.AddEntry(qsvunc,"Scale Variation","l")
            leg.AddEntry(q_sigma_tot,"Total","l")

            myText(0.18,0.9,"#it{#bf{#scale[1.8]{#bf{ATLAS} Internal}}}")

            leg.Draw()

            myText(0.18,0.86,"#bf{#scale[1.5]{#sqrt{s} = 13 TeV}}")
            myText(0.18,0.82,"#bf{#scale[1.5]{pT range: "+str(min)+" - "+str(max)+" GeV}}")
            myText(0.18,0.78,"#bf{#scale[1.5]{Quark jet}}")

            if(inputvar == "ntrk"):
                line = TLine(0.,0,60,0)
                quark_strap.GetXaxis().SetTitle("n_{Track}")
            if(inputvar == "bdt"):
                line = TLine(-0.8,0,0.7,0)
                quark_strap.GetXaxis().SetTitle("BDT")
#		line = TLine(0.,1,0.4,1)

#		quark_ratio.Draw()
            line.Draw("same")
            #c.Print("./plots_bdt/quark_"+str(min)+"_"+str(doreweight)+"_"+mc+"_"+var+"_fc.pdf")
            c.Print("./SF_plots_"+var+"/quark_"+str(min)+"_"+str(doreweight)+"_"+mc+"_"+var+".pdf")


            gluon_strap.GetYaxis().SetTitle("Uncertainty (%)")
            gluon_strap.GetYaxis().SetRangeUser(-50,50)


            gluon_strap.SetLineColor(2)
            gluon_strap.SetLineStyle(2)
            #gluon_strap.SetMarkerColor(2)
            #gluon_strap.SetMarkerSize(0.8)
            gluon_strap_negative.SetLineColor(2)
            gluon_strap_negative.SetLineStyle(2)
            #gluon_negative.SetMarkerColor(2)
            #gluon_negative.SetMarkerSize(0.8)


            gluon_use.SetLineColor(30)
            gluon_use.SetLineStyle(2)
            #gluon_use.SetMarkerColor(30)
            #gluon_use.SetMarkerSize(0.8)
            gluonMC_negative.SetLineColor(30)
            gluonMC_negative.SetLineStyle(2)
            #gluonMC_negative.SetMarkerColor(30)
            #gluonMC_negative.SetMarkerSize(0.8)


            gluon_pdf.SetLineColor(28)
            gluon_pdf.SetLineStyle(2)
            gluon_pdf_negative.SetLineColor(28)
            gluon_pdf_negative.SetLineStyle(2)

            g_show_unc.SetLineColor(1)
            g_show_unc.SetLineStyle(2)
            g_show_uncn.SetLineColor(1)
            g_show_uncn.SetLineStyle(2)

            ghadunc.SetLineColor(9)
            ghadunc.SetLineStyle(2)
            ghadn.SetLineColor(9)
            ghadn.SetLineStyle(2)

            gmeunc.SetLineColor(7)
            gmeunc.SetLineStyle(2)
            gmen.SetLineColor(7)
            gmen.SetLineStyle(2)

            gsvunc.SetLineColor(6)
            gsvunc.SetLineStyle(2)
            gsvn.SetLineColor(6)
            gsvn.SetLineStyle(2)

            g_sigma_tot.SetLineColor(4)
            g_sigma_tot.SetLineStyle(1)
            g_sigma_tot.SetLineWidth(2)
            g_sigma_tot_n.SetLineColor(4)
            g_sigma_tot_n.SetLineStyle(1)
            g_sigma_tot_n.SetLineWidth(2)

            gluon_strap.Draw("HIST")
            gluon_strap_negative.Draw("HIST same")
            gluon_use.Draw("HIST same")
            gluonMC_negative.Draw("HIST same")
            gluon_pdf.Draw("HIST same")
            gluon_pdf_negative.Draw("HIST same")
            g_show_unc.Draw("HIST same")
            g_show_uncn.Draw("HIST same")
            g_sigma_tot.Draw("HIST same")
            g_sigma_tot_n.Draw("HIST same")
            ghadunc.Draw("HIST same")
            ghadn.Draw("hist same")
            gmeunc.Draw("hist same")
            gmen.Draw("hist same")
            gsvunc.Draw("hist same")
            gsvn.Draw("hist same")

            leg = TLegend(0.82,0.7,0.98,0.9) ##0.6,0.5,0.9,0.7
            leg.SetTextFont(42)
            leg.SetFillColor(0)
            leg.SetBorderSize(0)
            leg.SetFillStyle(0)
            leg.SetNColumns(1)
            leg.AddEntry(gluon_strap,"Statistical","l")
            leg.AddEntry(gluon_use,"MC Closure","l")
            leg.AddEntry(gluon_pdf,"PDF","l")
            leg.AddEntry(g_show_unc,"Showering (herwig)","l")
            leg.AddEntry(qhadunc,"Hadronization","l")
            leg.AddEntry(gmeunc,"Matrix Element","l")
            leg.AddEntry(qsvunc,"Scale Variation","l")
            leg.AddEntry(g_sigma_tot,"Total","l")

            myText(0.18,0.9,"#it{#bf{#scale[1.8]{#bf{ATLAS} Internal}}}")

            leg.Draw()

            myText(0.18,0.86,"#bf{#scale[1.5]{#sqrt{s} = 13 TeV}}")
            myText(0.18,0.82,"#bf{#scale[1.5]{pT range: "+str(min)+" - "+str(max)+" GeV}}")
            myText(0.18,0.78,"#bf{#scale[1.5]{Gluon jet}}")

            if(inputvar == "ntrk"):
                line = TLine(0.,0,60,0)
                gluon_strap.GetXaxis().SetTitle("n_{Track}")
            if(inputvar == "bdt"):
                line = TLine(-0.8,0,0.7,0)
                gluon_strap.GetXaxis().SetTitle("BDT")

#		bot.cd()
#		gluon_ratio.Draw()
            line.Draw("same")
            c.Print("./SF_plots_"+var+"/gluon_"+str(min)+"_"+str(doreweight)+"_"+mc+"_"+var+".pdf")   
