#!/usr/bin/env python

import uproot3 as uproot
import sys
import datetime
import numpy as np

# This scripts read ttree as inputs and produce different histograms of distribution of variables in different pT range

#1.Select trigger "pass_HLT_j400
#2. select event :EMPFlowAntiKti4 jets with 1st pt :500-2000,   leading jet pt/sub-leading jet  pt < 1.5,  both jets |eta|<2.1    
#3.if the jets are MC, select leading and subleading reconstruction jets related to the truth jets. If jets are Data, skip this step .
#4.for each leading jet pT range, determinate forward or central assignment to 1st and 2nd jet, then categorise each jet as quark/gluon/other or  data
#5.fill hist with variables :Ntrk / BDT 
#

starttime = datetime.datetime.now()
print(starttime)

#bins = [300, 400, 500, 600, 800, 1000, 1200, 1500, 2000]  #this bin range is for only dijet event
bins = [0, 50, 100, 150, 200, 300, 400, 500, 600, 800, 1000, 1200, 1500, 2000]  #this bin range is for gammajet+dijet event
HistMap = {}
JetList = []

###### define functions
def GetHistBin(histname):
	if 'pt' in histname:
		return 60,0,2000
	elif 'eta' in histname:
		return 50,-2.5,2.5
	elif 'ntrk' in histname:
		return 60,0,60
	elif 'bdt' in histname:
		return 60,-0.8,0.7
	elif 'width' in histname:
		return 60,0.,0.4
	elif 'c1' in histname:
		return 60,0.,0.4

def FillTH1F(histname, var, w):
	if 'Data' in histname:
		w = 1
	if histname in HistMap:
		HistMap[histname][0].append(var)
		HistMap[histname][1].append(w) 
	else:
		HistMap[histname] = [[],[]] #The first list is for the data, the second for the weights
		HistMap[histname] = [[],[]]
		HistMap[histname][0].append(var)
		HistMap[histname][1].append(w)

def FillHisto(prefix, jetlist, w):
	FillTH1F(prefix+"_ntrk", jetlist[0], w)
	FillTH1F(prefix+"_bdt", jetlist[1], w)
	FillTH1F(prefix+"_width", jetlist[2], w)
	FillTH1F(prefix+"_c1", jetlist[3], w)
	FillTH1F(prefix+"_pt", jetlist[4], w)
	FillTH1F(prefix+"_eta", jetlist[5], w)


def GetJetType(label):
	if label == -99:
		return "Data"
	elif label == 21:
		return "Gluon"
	elif label > 0 and label < 5:
		return "Quark"
	else:
		return "Other"


def FindBinIndex(jet_pt,ptbin):
	for j in range(len(ptbin)-1):
		if jet_pt >= ptbin[j] and jet_pt < ptbin[j+1]:
			return ptbin[j]

	print("error: jet pT outside the bin range")
	return -1

#Unfourtunately I can't fully utilize the use of arrays because each jet must be matched with the corresponding histogram.
#for i in range():
def ReadTree(df):
    for i in range(0,len(df[b"pass_HLT_j400"])):
        if(df[b"pass_HLT_j400"][i] == 1 and df[b"j1_pT"][i] > 400 and df[b"j1_pT"][i] < 2000 and df[b"j3_pT"][i] >20 and abs(df[b"j1_eta"][i]) < 2.1 and abs(df[b"j2_eta"][i]) < 2.1 and abs(df[b"j3_eta"][i]) < 2.1 and df[b"j1_pT"][i]/df[b"j2_pT"][i] < 1.5):
                
            pTbin1 = FindBinIndex(df[b"j1_pT"][i], bins)
            pTbin2 = FindBinIndex(df[b"j2_pT"][i], bins)
            pTbin3 = FindBinIndex(df[b"j3_pT"][i], bins)

            label1 = GetJetType(df[b"j1_partonLabel"][i])
            label2 = GetJetType(df[b"j2_partonLabel"][i])
            label3 = GetJetType(df[b"j3_partonLabel"][i])
            eta1 = "Central"
            eta2 = "Central"
            eta3 = "Central"
            
            JetList = [[df[b"j1_NumTrkPt500"][i], df[b"j1_bdt_resp"][i], df[b"j1_trackWidth"][i], df[b"j1_trackC1"][i], df[b"j1_pT"][i], df[b"j1_eta"][i]],[df[b"j2_NumTrkPt500"][i],df[b"j2_bdt_resp"][i], df[b"j2_trackWidth"][i], df[b"j2_trackC1"][i], df[b"j2_pT"][i], df[b"j2_eta"][i]],[df[b"j3_NumTrkPt500"][i], df[b"j3_bdt_resp"][i], df[b"j3_trackWidth"][i], df[b"j3_trackC1"][i], df[b"j3_pT"][i], df[b"j3_eta"][i]]]
            	
            total_weight = df[b"weight"][i]*df[b"filter_efficiency"][i]*df[b"xsection"][i]/df[b"mc_weight"][i]
            	
            FillHisto(str(pTbin1)+"_j1_"+eta1+"_"+label1, JetList[0], total_weight)
            FillHisto(str(pTbin2)+"_j2_"+eta2+"_"+label2, JetList[1], total_weight)
            FillHisto(str(pTbin2)+"_j3_"+eta3+"_"+label3, JetList[2], total_weight)


######## read and excute TTree from root file 
#finput = TFile.Open("/eos/user/e/esaraiva/AQT_dijet_sherpa_bdt/dijet_sherpa_bdt_d.root")
finput = uproot.open("/afs/cern.ch/work/r/rqian/public/gamma2jet/rootfiles/dijet-pythia-bdt.root")["AntiKt4EMPFlow_dijet_insitu"]
runtime = datetime.datetime.now() - starttime
print("Done loading tree [ "+str(runtime)+" ]")
print(finput.numentries)

#for i in range (0,loopcount):
#    print("Starting loop "+str(i)+"; Data entries "+str(i*datapoints)+" to "+str((i+1)*datapoints))
#    data = finput.arrays(["pass_HLT_j400","j[12]_pT","j[12]_eta","j[12]_NumTrkPt500","j[12]_bdt_resp","pdfWeights","j[12]_trackWidth","j[12]_trackC1","weight","weight_ptslice","j[12]_partonLabel"],entrystart=i*datapoints,entrystop=(i+1)*datapoints)#this converts the tree to a python dictionary with keys equal to the branch names and values equal to the branch data. This can be left blank to iport all of the data from all branches.
#    ReadTree(data)

data = finput.arrays(["pass_HLT_j400","j[123]_pT","j[123]_eta","j[123]_NumTrkPt500","j[123]_bdt_resp","weight","j[123]_trackWidth","j[123]_trackC1","filter_efficiency","xsection","mc_weight","j[123]_partonLabel"],entrystart=int(sys.argv[1]),entrystop=int(sys.argv[2])) #this converts the tree to a python dictionary with keys equal to the branch names and values equal to the branch data. This can be left blank to iport all of the data from all branches.
ReadTree(data)
runtime = datetime.datetime.now() - starttime
print("Completed Loop [ "+str(runtime)+" ]")

foutput = uproot.recreate(sys.argv[3])

#Create the actual histograms now that the data is in separate lists
#uproot lets you use numpy histograms and write them to root files.
for hist in HistMap.keys():
    nbin,binmin,binmax = GetHistBin(hist)
    histogram = np.histogram(a = HistMap[hist][0], weights = HistMap[hist][1], bins = nbin, range = (binmin,binmax))
    foutput[hist] = histogram

#foutput = TFile("dijet_sherpa_py.root","recreate")
runtime = datetime.datetime.now() - starttime
print("Done. Total runtime: "+str(runtime))
