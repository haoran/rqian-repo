#!/usr/bin/var python
import uproot3 as uproot
import sys

num_loops = 64
sample = "dijet_pythia" #used in the name of the submit file.
input_path = "../rootfiles/dijet-pythia-bdt.root" #the root file that will be analyzed
output_path = "/afs/cern.ch/work/r/rqian/public/gamma2jet/uproot" #the folder where the histograms will be written

merge_lines = ["#!/bin/bash \n \nhadd -f "+sample+"_py.root "]
f = open(sample+".sub","w+")
lines = ["#HTCondor submit file for "+sample+" \n","executable = ReadingTree.sh \n","input = ReadingTreeMC.py \n",'+JobFlavour = "longlunch" \n',"arguments = $(ClusterId)$(ProcId) \n","output = output/"+sample+".$(ClusterId).$(ProcId).out \n","error = error/"+sample+".$(ClusterId).$(ProcId).err \n","log = log/"+sample+".$(ClusterId).log \n \n"]

f.writelines(lines)

finput = uproot.open(str(input_path))["AntiKt4EMPFlow_dijet_insitu"]
proc_ent = int(finput.numentries/num_loops)

for i in range(0,num_loops-1):
	entrystart = i*proc_ent
	entrystop = (i+1)*proc_ent
	merge_lines.append('diijet-pythia-uproot-'+str(i)+'.root ')
	lines = ['arguments = "'+str(entrystart)+' '+str(entrystop-1)+' '+str(output_path)+'/dijet-pythia-uproot-'+str(i)+'.root" \n',"queue \n \n"]
	f.writelines(lines)

entrystart = (num_loops-1)*proc_ent
entrystop = finput.numentries

merge_lines.append('dijet-pythia-uproot-'+str(num_loops-1)+'.root ')
lines = ['arguments = "'+str(entrystart)+' '+str(entrystop)+' '+str(output_path)+'/dijet-pythia-uproot-'+str(num_loops-1)+'.root" \n',"queue \n \n"]
f.writelines(lines)

f = open("merge.sh","w+")
f.writelines(merge_lines)
