# Using Uproot

## Setting up the environment

Execute the following commands in a directory you will remember, or the working directory to install uproot to a virtual environment.

	python3 -m venv env
	source env/bin/activate
	pip install uproot3

## Preparing to run

Due to memory limits and high cpu usage, lxplus cannot be used, so we must use condor. ReadingTreeCondor.py will write a submit file for you with the necessary arguments and also a script to merge the histograms.

Inside ReadingTreeCondor.py, change the sample, input_path and output_path to your file locations. Change the loop count to the desired amount. Each loop corresponds to one condor process and one additional output root file. If this number is too low, the condor process could run out of memory.

Inside ReadingTree.sh, change source line to the path of your virtual environment.

Run ReadingTreeCondor.py

	python ReadingTreeCondor.py

copy the submit file, ReadingTreeMC.py, and ReadingTree.sh to a directory on /afs/.

## Executing the Job

Submit the job using

	condor_submit (sample).sub

where (sample) is the same name that was specified in ReadingTreeCondor.py

The condor job takes ReadingTree.sh as the executable and ReadingTreeMC.py as an input. The bash script will source the virtual environment with uproot on it and execute ReadingTree.py after. The output files are written in the specified path in ReadingTreeCondor.py

## Merge the output files

ReadingTreeCondor.py should also generate a file called merge.sh, which will write the necessary command to merge all of the output files together into one. Run this using

	merge.sh

 The resulting file can be used for analysis.
